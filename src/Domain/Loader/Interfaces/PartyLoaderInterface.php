<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\PartyOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface PartyLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return PartyOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
