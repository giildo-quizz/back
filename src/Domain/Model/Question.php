<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\QuestionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Question
 * @ORM\Table(name="api_question")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\QuestionRepository")
 */
class Question implements QuestionInterface
{
    const TYPE_IMG = 1;
    const TYPE_VID = 2;
    const TYPE_TEXT = 3;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $type;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $text;
    /**
     * @var string
     * @ORM\Column(type="json_array")
     */
    private $definition;
    /**
     * @var GameInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Game", cascade={"persist"}, inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * Question constructor.
     *
     * @param string        $type
     * @param string        $text
     * @param string        $definition
     * @param GameInterface $game
     */
    public function __construct(
        string $type,
        string $text,
        string $definition,
        GameInterface $game
    ) {
        $this->type = $type;
        $this->text = $text;
        $this->definition = $definition;
        $this->game = $game;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        $type = '';
        switch ($this->type) {
            case $this::TYPE_IMG:
                $type = 'image';
                break;
            case $this::TYPE_VID:
                $type = 'video';
                break;
            case $this::TYPE_TEXT:
                $type = 'text';
                break;
        }

        return $type;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getDefinition(): string
    {
        return $this->definition;
    }

    /**
     * @param string|null $text
     * @param string|null $definition
     */
    public function updateQuestion(
        ?string $text = '',
        ?string $definition = ''
    ): void {
        $this->text = $text !== '' ? $text : $this->text;
        $this->definition = $definition !== '' ? $definition : $this->definition;
    }
}
