<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\ModelInterface;

interface OutputInterface extends OutInterface
{
    /**
     * @return ModelInterface|null
     */
    public function getItem(): ?ModelInterface;
}
