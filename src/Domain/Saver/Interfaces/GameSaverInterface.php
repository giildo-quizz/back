<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\ORMException;

interface GameSaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return GameOutputInterface
     * @throws ORMException
     */
    public function save(string $credentials): OutInterface;
}
