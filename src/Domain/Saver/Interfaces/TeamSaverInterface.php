<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamOutputInterface;

interface TeamSaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return TeamOutputInterface
     */
    public function save(string $credentials): OutInterface;
}
