<?php

namespace App\Domain\Model\Interfaces;

use DateTime;

interface GameInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return bool
     */
    public function isClosed(): bool;

    /**
     * @return bool
     */
    public function isPublished(): bool;

    /**
     * @return bool
     */
    public function isStarted(): bool;

    /**
     * @return QuestionInterface[]
     */
    public function getQuestions(): array;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime;

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime;

    /**
     * @return void
     */
    public function close(): void;

    /**
     * @param bool $isPublished
     *
     * @return void
     */
    public function changePublishStatus(bool $isPublished): void;

    /**
     * @param string|null $title
     * @param string|null $description
     */
    public function updateGame(
        ?string $title = '',
        ?string $description = ''
    ): void;

    /**
     * @return void
     */
    public function changeDates(): void;

    /**
     * @return void
     */
    public function start(): void;

    /**
     * @return void
     */
    public function stop(): void;
}