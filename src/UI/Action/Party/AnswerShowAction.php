<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\AnswerLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnswerShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var AnswerLoaderInterface
     */
    private $answerLoader;

    /**
     * AnswerCreateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param AnswerLoaderInterface $answerLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        AnswerLoaderInterface $answerLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->answerLoader = $answerLoader;
    }

    /**
     * @Route(
     *     path="/api/answers/team/{teamId}/party/{partyId}/question/{questionId}",
     *     methods={"GET"},
     *     name="answer_show"
     * )
     * @param Request $request
     * @param int     $teamId
     * @param int     $partyId
     * @param int     $questionId
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function answerShowAction(
        Request $request,
        int $teamId,
        int $partyId,
        int $questionId
    ): Response {
        return $this->apiResponder->response(
            $this->answerLoader->load(
                null,
                [
                    'partyId'    => $partyId,
                    'questionId' => $questionId,
                    'teamId'     => $teamId,
                    'list'       => false
                ]
            ),
            $request
        );
    }
}
