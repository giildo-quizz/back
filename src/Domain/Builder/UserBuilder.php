<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\UserBuilderInterface;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\UserDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\UserInterface;
use App\Domain\Model\User;

class UserBuilder implements UserBuilderInterface
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @param UserDTO $dto
     * @param array|null $params
     *
     * @return UserBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->user = new User(
            $dto->directoryId,
            $dto->username,
            $dto->mail,
            $dto->firstName,
            $dto->lastName
        );

        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->user;
    }
}
