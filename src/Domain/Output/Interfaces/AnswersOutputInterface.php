<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\AnswerInterface;

interface AnswersOutputInterface extends OutputsInterface
{
    /**
     * @return AnswerInterface[]
     */
    public function getItems(): array;
}
