<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\PartyDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\PartyInterface;
use Doctrine\ORM\NonUniqueResultException;

interface PartyBuilderInterface extends BuilderInterface
{
    /**
     * @param PartyDTO   $dto
     * @param array|null $params
     *
     * @return PartyBuilderInterface
     * @throws NonUniqueResultException
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return PartyInterface
     */
    public function getModel(): ModelInterface;
}
