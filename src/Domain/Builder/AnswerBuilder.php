<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\AnswerBuilderInterface;
use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\DTO\AnswerDTO;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Answer;
use App\Domain\Model\Interfaces\AnswerInterface;
use App\Domain\Model\Interfaces\ModelInterface;

class AnswerBuilder implements AnswerBuilderInterface
{
    /**
     * @var AnswerInterface
     */
    private $answer;

    /**
     * @param AnswerDTO $dto
     * @param array|null $params
     *
     * @return AnswerBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->answer = new Answer(
            $dto->partyId,
            $dto->questionId,
            $dto->teamId,
            $dto->value
        );

        return $this;
    }

    /**
     * @return AnswerInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->answer;
    }
}
