<?php


namespace App\Domain\Updater;


use App\Domain\Output\GameOutput;
use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\GameRepository;
use App\Domain\Updater\Interfaces\GameStartUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;

class GameStartUpdater implements GameStartUpdaterInterface
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameStartUpdater constructor.
     *
     * @param GameRepository $gameRepository
     */
    public function __construct(
        GameRepository $gameRepository
    ) {
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param string     $credentials
     * @param int|string $id
     *
     * @return GameOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface {
        $game = $this->gameRepository->loadGameById($id);
        $credentials === 'start'
            ? $game->start()
            : $game->stop();
        $this->gameRepository->saveGame($game);

        return new GameOutput($game);
    }
}