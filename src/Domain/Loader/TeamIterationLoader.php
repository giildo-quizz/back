<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\TeamIterationLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamsOutputInterface;
use App\Domain\Output\TeamsOutput;
use App\Domain\Repository\InstanceRepository;
use App\Domain\Repository\PartyRepository;
use Doctrine\ORM\NonUniqueResultException;

class TeamIterationLoader implements TeamIterationLoaderInterface
{
    /**
     * @var InstanceRepository
     */
    private $instanceRepository;
    /**
     * @var PartyRepository
     */
    private $partyRepository;

    /**
     * TeamIterationLoader constructor.
     *
     * @param InstanceRepository $instanceRepository
     * @param PartyRepository    $partyRepository
     */
    public function __construct(
        InstanceRepository $instanceRepository,
        PartyRepository $partyRepository
    ) {
        $this->instanceRepository = $instanceRepository;
        $this->partyRepository = $partyRepository;
    }

    /**
     * @param int|null   $id - Identifiant de l'instance
     * @param array|null $options
     *
     * @return TeamsOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        return new TeamsOutput(
            $this->partyRepository->loadPartiesNotClosedByGameId(
                $this->instanceRepository->loadInstanceById($id)->getGame()->getId()
            )
        );
    }
}
