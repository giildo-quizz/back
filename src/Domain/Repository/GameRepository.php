<?php

namespace App\Domain\Repository;

use App\Domain\Model\Game;
use App\Domain\Model\Interfaces\GameInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @return GameInterface[]
     */
    public function loadGames(): array
    {
        return $this->createQueryBuilder('g')
                    ->leftJoin('g.questions', 'questions')
                    ->addSelect('questions')
                    ->where('g.closed = :closed')
                    ->setParameter('closed', false)
                    ->getQuery()
                    ->getResult();
    }


    /**
     * @param GameInterface $game
     *
     * @throws ORMException
     */
    public function saveGame(GameInterface $game): void
    {
        $this->_em->persist($game);
        $this->_em->flush();
    }

    /**
     * @param int|null $id
     *
     * @return GameInterface|null
     * @throws NonUniqueResultException
     */
    public function loadGameById(?int $id): ?GameInterface
    {
        return $this->createQueryBuilder('g')
                    ->leftJoin('g.questions', 'questions')
                    ->addSelect('questions')
                    ->where('g.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function closeGame(int $id): void
    {
        $game = $this->loadGameById($id);
        $game->close();
        $this->_em->flush();
    }

    /**
     * @param array $roles
     *
     * @return GameInterface[]
     */
    public function loadGamePublished(array $roles): array
    {
        $qb = $this->createQueryBuilder('g');

        in_array('ROLE_ADMIN', $roles)
            ? $qb->where('g.published = :published')
                 ->setParameter('published', true)
            : $qb->where('g.started = :started')
                 ->setParameter('started', true);

        return $qb->getQuery()
                  ->getResult();
    }
}
