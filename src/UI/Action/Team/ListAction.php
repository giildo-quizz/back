<?php

namespace App\UI\Action\Team;

use App\Domain\Loader\Interfaces\TeamLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamLoaderInterface
     */
    private $teamLoader;

    /**
     * ListAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param TeamLoaderInterface   $teamLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamLoaderInterface $teamLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamLoader = $teamLoader;
    }

    /**
     * @Route(
     *     path="/api/teams",
     *     name="team_list",
     *     methods={"GET"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function list(Request $request): Response
    {
        return $this->apiResponder->response($this->teamLoader->load(), $request);
    }
}
