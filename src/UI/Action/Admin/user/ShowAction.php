<?php


namespace App\UI\Action\Admin\user;


use App\Domain\Loader\Interfaces\UserLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var UserLoaderInterface
     */
    private $userLoader;

    /**
     * ShowAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param UserLoaderInterface   $userLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        UserLoaderInterface $userLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->userLoader = $userLoader;
    }

    /**
     * @Route(
     *     path="/api/users",
     *     methods={"GET"},
     *     name="user_show"
     * )
     * @param Request $request
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(Request $request): Response
    {
        return $this->apiResponder->response(
            $this->userLoader->load(
                null,
                [
                    'user' => explode(' ', $request->headers->get('Authorization'))[1],
                ]
            ),
            $request
        );
    }
}