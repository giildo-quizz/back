<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\TeamDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TeamDTO implements TeamDTOInterface
{
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $name;
    /**
     * @var array|string[]
     * @Assert\Type(type="array")
     */
    public $gamers;

    /**
     * TeamDTO constructor.
     *
     * @param string|null $name
     * @param string[]    $gamers
     */
    public function __construct(
        ?string $name = '',
        ?array $gamers = []
    ) {
        $this->name = $name;
        $this->gamers = $gamers;
    }
}
