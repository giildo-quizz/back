<?php

namespace App\Domain\Model\Interfaces;

interface AnswerInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getParty(): int;

    /**
     * @return int
     */
    public function getQuestion(): int;

    /**
     * @return int
     */
    public function getTeam(): int;

    /**
     * @return string
     */
    public function getValue(): string;

    /**
     * @param string $value
     */
    public function setValue(string $value): void;
}