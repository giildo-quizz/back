<?php

namespace App\Domain\Deleter;

use App\Domain\Deleter\Interfaces\GameDeleterInterface;
use App\Domain\Repository\GameRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class GameDeleter implements GameDeleterInterface
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameDeleter constructor.
     *
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete($id): void
    {
        $this->gameRepository->closeGame($id);
    }
}
