<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface BuilderInterface
{
    /**
     * @param DTOInterface $dto
     * @param array|null   $params
     *
     * @return BuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface;
}
