<?php


namespace App\Domain\Updater\Interfaces;


use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;

interface GameStartUpdaterInterface extends UpdaterInterface
{
    /**
     * @param string     $credentials
     * @param int|string $id
     *
     * @return GameOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface;
}