<?php

namespace App\Domain\Deleter\Interfaces;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface QuestionDeleterInterface extends DeleterInterface
{
    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete($id): void;
}
