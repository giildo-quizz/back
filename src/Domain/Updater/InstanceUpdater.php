<?php

namespace App\Domain\Updater;

use App\Domain\Output\InstanceOutput;
use App\Domain\Output\Interfaces\InstanceOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\InstanceRepository;
use App\Domain\Repository\QuestionRepository;
use App\Domain\Updater\Interfaces\InstanceUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class InstanceUpdater implements InstanceUpdaterInterface
{
    /**
     * @var InstanceRepository
     */
    private $instanceRepository;
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * InstanceUpdater constructor.
     *
     * @param InstanceRepository $instanceRepository
     * @param QuestionRepository $questionRepository
     */
    public function __construct(
        InstanceRepository $instanceRepository,
        QuestionRepository $questionRepository
    ) {
        $this->instanceRepository = $instanceRepository;
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param string     $credentials
     * @param int|string $id
     *
     * @return InstanceOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface {
        $instance = $this->instanceRepository->loadInstanceById($id);
        $instance->updateQuestion($this->questionRepository->loadQuestionById($credentials));
        $this->instanceRepository->save($instance);
        return new InstanceOutput($instance);
    }
}
