<?php

namespace App\Domain\Updater\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;

interface UpdaterInterface
{
    /**
     * @param string $credentials
     * @param string|int $id
     *
     * @return OutInterface
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface;
}
