<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\TeamInterface;
use App\Domain\Output\Interfaces\TeamOutputInterface;

class TeamOutput implements TeamOutputInterface
{
    /**
     * @var TeamInterface
     */
    private $team;

    /**
     * TeamOutput constructor.
     *
     * @param TeamInterface $team
     */
    public function __construct(TeamInterface $team)
    {
        $this->team = $team;
    }

    /**
     * @return TeamInterface|null
     */
    public function getItem(): ?ModelInterface
    {
        return $this->team;
    }
}
