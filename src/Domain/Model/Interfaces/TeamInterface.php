<?php

namespace App\Domain\Model\Interfaces;

interface TeamInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return array|string[]
     */
    public function getGamers(): array;

    /**
     * @param string $name
     * @param array  $gamers
     */
    public function updateTeam(
        string $name,
        array $gamers
    ): void;
}