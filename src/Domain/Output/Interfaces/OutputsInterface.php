<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\ModelInterface;

interface OutputsInterface extends OutInterface
{
    /**
     * @return ModelInterface[]
     */
    public function getItems(): array;
}
