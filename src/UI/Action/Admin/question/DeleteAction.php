<?php

namespace App\UI\Action\Admin\question;

use App\Domain\Deleter\Interfaces\QuestionDeleterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var QuestionDeleterInterface
     */
    private $questionDeleter;

    /**
     * DeleteAction constructor.
     *
     * @param ApiResponderInterface    $apiResponder
     * @param QuestionDeleterInterface $questionDeleter
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        QuestionDeleterInterface $questionDeleter
    ) {
        $this->apiResponder = $apiResponder;
        $this->questionDeleter = $questionDeleter;
    }

    /**
     * @Route(
     *     path="/api/admin/questions/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"},
     *     name="question_delete"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(
        Request $request,
        int $id
    ): Response {
        $this->questionDeleter->delete($id);
        return $this->apiResponder->response(
            null,
            $request,
            Response::HTTP_NO_CONTENT
        );
    }
}
