<?php

namespace App\UI\Action\Team;

use App\Domain\Deleter\Interfaces\TeamDeleterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamDeleterInterface
     */
    private $teamDeleter;

    /**
     * DeleteAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param TeamDeleterInterface  $teamDeleter
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamDeleterInterface $teamDeleter
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamDeleter = $teamDeleter;
    }

    /**
     * @Route(
     *     path="/api/teams/{id}",
     *     requirements={"id"="\d+"},
     *     name="team_delete",
     *     methods={"DELETE"}
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Request $request, int $id): Response
    {
        $this->teamDeleter->delete($id);
        return $this->apiResponder->response(null, $request, Response::HTTP_NO_CONTENT);
    }
}
