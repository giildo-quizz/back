<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\QuestionInterface;
use App\Domain\Model\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;

class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * @param QuestionInterface $question
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveQuestion(QuestionInterface $question): void
    {
        $this->_em->persist($question);
        $this->_em->flush();
    }

    /**
     * @param int $id
     *
     * @return QuestionInterface
     * @throws NonUniqueResultException
     */
    public function loadQuestionById(int $id): QuestionInterface
    {
        return $this->createQueryBuilder('q')
                    ->where('q.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NonUniqueResultException
     */
    public function deleteQuestion(int $id): void
    {
        $this->_em->remove($this->loadQuestionById($id));
        $this->_em->flush();
    }
}
