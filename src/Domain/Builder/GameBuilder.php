<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\GameBuilderInterface;
use App\Domain\DTO\GameDTO;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Game;
use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\ModelInterface;

class GameBuilder implements GameBuilderInterface
{
    /**
     * @var GameInterface
     */
    private $game;

    /**
     * @param GameDTO $dto
     * @param array|null       $params
     *
     * @return GameBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->game = new Game(
            $dto->title,
            $dto->description,
            $dto->closed,
            $dto->published
        );

        return $this;
    }

    /**
     * @return GameInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->game;
    }
}
