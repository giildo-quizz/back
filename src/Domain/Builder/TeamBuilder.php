<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\TeamBuilderInterface;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\TeamDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\TeamInterface;
use App\Domain\Model\Team;

class TeamBuilder implements TeamBuilderInterface
{
    /**
     * @var TeamInterface
     */
    private $team;

    /**
     * @param TeamDTO $dto
     * @param array|null $params
     *
     * @return TeamBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->team = new Team(
            $dto->name,
            $dto->gamers
        );

        return $this;
    }

    /**
     * @return TeamInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->team;
    }
}
