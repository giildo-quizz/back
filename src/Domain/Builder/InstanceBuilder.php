<?php


namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\InstanceBuilderInterface;
use App\Domain\DTO\InstanceDTO;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Instance;
use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\ModelInterface;

class InstanceBuilder implements InstanceBuilderInterface
{
    /**
     * @var InstanceInterface
     */
    private $instance;

    /**
     * @param InstanceDTO $dto
     * @param array|null   $params
     *
     * @return InstanceBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->instance = new Instance(
            $dto->game,
            $dto->question
        );

        return $this;
    }

    /**
     * @return InstanceInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->instance;
    }
}