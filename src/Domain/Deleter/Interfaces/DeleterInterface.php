<?php

namespace App\Domain\Deleter\Interfaces;

interface DeleterInterface
{
    /**
     * @param string|int $id
     */
    public function delete($id): void;
}
