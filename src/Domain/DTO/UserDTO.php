<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\UserDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserDTO implements UserDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $directoryId;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     max="50",
     *     minMessage="Le nom d'utilisateur doit faire moins que {{ limit }} caractères."
     * )
     */
    public $username;
    /**
     * @var string|null
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     max="255",
     *     minMessage="Le mail doit faire moins que {{ limit }} caractères."
     * )
     */
    public $mail;
    /**
     * @var string|null
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     max="50",
     *     minMessage="Le prénom doit faire moins que {{ limit }} caractères."
     * )
     */
    public $firstName;
    /**
     * @var string|null
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     max="50",
     *     minMessage="Le nom doit faire moins que {{ limit }} caractères."
     * )
     */
    public $lastName;

    /**
     * UserDTO constructor.
     *
     * @param string|null $directoryId
     * @param string|null $username
     * @param string|null $mail
     * @param string|null $firstName
     * @param string|null $lastName
     */
    public function __construct(
        ?string $directoryId = null,
        ?string $username = null,
        ?string $mail = null,
        ?string $firstName = null,
        ?string $lastName = null
    ) {
        $this->directoryId = $directoryId;
        $this->username = $username;
        $this->mail = $mail;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }
}
