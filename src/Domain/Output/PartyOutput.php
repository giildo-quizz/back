<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\PartyInterface;
use App\Domain\Output\Interfaces\PartyOutputInterface;

class PartyOutput implements PartyOutputInterface
{
    /**
     * @var PartyInterface
     */
    private $party;

    /**
     * PartyOutput constructor.
     *
     * @param PartyInterface $party
     */
    public function __construct(PartyInterface $party)
    {
        $this->party = $party;
    }

    /**
     * @return PartyInterface|null
     */
    public function getItem(): ?ModelInterface
    {
        return $this->party;
    }
}
