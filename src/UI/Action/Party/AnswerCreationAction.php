<?php

namespace App\UI\Action\Party;

use App\Domain\Saver\Interfaces\AnswerSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnswerCreationAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var AnswerSaverInterface
     */
    private $answerSaver;

    /**
     * AnswerCreationAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param AnswerSaverInterface  $answerSaver
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        AnswerSaverInterface $answerSaver
    ) {
        $this->apiResponder = $apiResponder;
        $this->answerSaver = $answerSaver;
    }

    /**
     * @Route(
     *     path="/api/answers",
     *     methods={"POST"},
     *     name="answer_creation"
     * )
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function answerCreation(Request $request): Response
    {
        return $this->apiResponder->response(
            $this->answerSaver->save($request->getContent()),
            $request,
            Response::HTTP_CREATED
        );
    }
}
