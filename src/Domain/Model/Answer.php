<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\AnswerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Answer
 * @ORM\Table(name="api_answer")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\AnswerRepository")
 */
class Answer implements AnswerInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Party")
     * @ORM\Column(nullable=false)
     */
    private $party;
    /**
     * @var int
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Question")
     * @ORM\Column(nullable=false)
     */
    private $question;
    /**
     * @var int
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Team")
     * @ORM\Column(nullable=false)
     */
    private $team;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * Answer constructor.
     *
     * @param int    $party
     * @param int    $question
     * @param int    $team
     * @param string $value
     */
    public function __construct(
        int $party,
        int $question,
        int $team,
        string $value
    ) {
        $this->party = $party;
        $this->question = $question;
        $this->team = $team;
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getParty(): int
    {
        return $this->party;
    }

    /**
     * @return int
     */
    public function getQuestion(): int
    {
        return $this->question;
    }

    /**
     * @return int
     */
    public function getTeam(): int
    {
        return $this->team;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
