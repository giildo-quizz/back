<?php


namespace App\Domain\Saver\Interfaces;


use App\Domain\Output\Interfaces\InstanceOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface InstanceSaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return InstanceOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NonUniqueResultException
     */
    public function save(string $credentials): OutInterface;
}