<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\UserDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\UserInterface;

interface UserBuilderInterface extends BuilderInterface
{
    /**
     * @param UserDTO    $dto
     * @param array|null $params
     *
     * @return UserBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return UserInterface
     */
    public function getModel(): ModelInterface;
}
