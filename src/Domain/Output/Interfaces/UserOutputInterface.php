<?php


namespace App\Domain\Output\Interfaces;


use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\UserInterface;

interface UserOutputInterface extends OutputInterface
{
    /**
     * @return UserInterface|null
     */
    public function getItem(): ?ModelInterface;
}