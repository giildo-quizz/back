<?php

namespace App\Domain\Model\Interfaces;

interface UserInterface extends ModelInterface
{
    /**
     * @return string
     */
    public function getDirectoryId(): string;

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return string|null
     */
    public function getMail(): ?string;

    /**
     * @return array
     */
    public function getRoles(): array;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;
}