<?php

namespace App\Application\Listener;

use App\Application\Helper\ExceptionOutput;
use App\Application\Helper\JsonWebToken;
use App\Application\Helper\TokenException;
use App\Domain\Builder\Interfaces\UserBuilderInterface;
use App\Domain\DTO\UserDTO;
use App\Domain\Repository\UserRepository;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TokenAuthenticationListener
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var UserBuilderInterface
     */
    private $userBuilder;

    /**
     * TokenAuthenticationListener constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param UserRepository        $userRepository
     * @param ValidatorInterface    $validator
     * @param UserBuilderInterface  $userBuilder
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        UserRepository $userRepository,
        ValidatorInterface $validator,
        UserBuilderInterface $userBuilder
    ) {
        $this->apiResponder = $apiResponder;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
        $this->userBuilder = $userBuilder;
    }

    /**
     * @param RequestEvent $event
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if (!$request->headers->get('Authorization')) {
            $event->setResponse(
                $this->apiResponder->response(
                    new ExceptionOutput('Autorisation nécessaire.', TokenException::NO_TOKEN),
                    $request,
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $token = new JsonWebToken(
            preg_replace(
                '/Bearer /',
                '',
                $request->headers->get('Authorization')
            )
        );

        if (!$token->isValid() || !$token->isAccess()) {
            $event->setResponse(
                $this->apiResponder->response(
                    new ExceptionOutput('Le token n\'est pas valide.', TokenException::TOKEN_INVALID),
                    $request,
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        if ($token->isExpired()) {
            $event->setResponse(
                $this->apiResponder->response(
                    new ExceptionOutput('Le token est expiré.', TokenException::TOKEN_INVALID),
                    $request,
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $user = $this->userRepository->loadUserByUid($token->getUid());

        if (is_null($user)) {
            $userDTO = new UserDTO(
                $token->getUid(),
                $token->getUsername(),
                $token->getMail(),
                $token->getFirstName(),
                $token->getLastName()
            );

            $this->validator->validate($userDTO);

            // TODO: Gestion des erreurs

            $this->userRepository->saveUser(
                $this->userBuilder->build($userDTO)
                                  ->getModel()
            );
        }

        return;
    }
}
