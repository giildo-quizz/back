<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\AnswerInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface AnswerOutputInterface extends OutputInterface
{
    /**
     * @return AnswerInterface|null
     */
    public function getItem(): ?ModelInterface;
}
