<?php

namespace App\Application\Helper\Interfaces;

interface JsonWebTokenInterface
{
    /**
     * @return bool
     */
    public function isAccess(): bool;

    /**
     * @return bool
     */
    public function isRefresh(): bool;

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return bool
     */
    public function isExpired();

    /**
     * @param string $username
     *
     * @return string
     */
    public function isGoodUser(string $username);

    /**
     * @return string
     */
    public function getUid(): string;

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * @return string|null
     */
    public function getMail(): ?string;
}
