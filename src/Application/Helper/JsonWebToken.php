<?php

namespace App\Application\Helper;

use App\Application\Helper\Interfaces\JsonWebTokenInterface;

class JsonWebToken implements JsonWebTokenInterface
{
    /**
     * @var string
     */
    private $header;
    /**
     * @var string
     */
    private $payload;
    /**
     * @var string
     */
    private $signature;
    /**
     * @var string
     */
    private $type;
    /**
     * @var int
     */
    private $iat;
    /**
     * @var int
     */
    private $exp;
    /**
     * @var string
     */
    private $uid;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string|null
     */
    private $firstName;
    /**
     * @var string|null
     */
    private $lastName;
    /**
     * @var string|null
     */
    private $mail;

    /**
     * JsonWebToken constructor.
     *
     * @param string $jwt
     */
    public function __construct(string $jwt)
    {
        $jwtArray = preg_split('/\./', $jwt);
        $this->header = $jwtArray[0];
        $this->payload = $jwtArray[1];
        $this->signature = $jwtArray[2];

        $payload = json_decode(base64_decode($this->payload));
        $this->type = $payload->type;
        $this->iat = $payload->iat;
        $this->exp = $payload->exp;
        $this->uid = $payload->uid;
        $this->username = $payload->username;
        $this->firstName = $payload->firstName;
        $this->lastName = $payload->lastName;
        $this->mail = $payload->mail;
    }

    /**
     * @return bool
     */
    public function isAccess(): bool
    {
        return $this->type === 'access';
    }

    /**
     * @return bool
     */
    public function isRefresh(): bool
    {
        return $this->type === 'refresh';
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        $signature = hash_hmac(
            'sha256',
            $this->header . "." . $this->payload,
            $_ENV['JWT_SECRET_PASS'],
            true
        );

        return $this->signature === str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return time() > $this->exp;
    }

    /**
     * @param string $username
     *
     * @return string
     */
    public function isGoodUser(string $username)
    {
        return $username === $this->username;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getMail(): ?string
    {
        return $this->mail;
    }
}
