<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\GameDTO;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface GameBuilderInterface extends BuilderInterface
{
    /**
     * @param GameDTO $dto
     * @param array|null   $params
     *
     * @return GameBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return GameInterface
     */
    public function getModel(): ModelInterface;
}
