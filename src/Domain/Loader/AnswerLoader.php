<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\AnswerLoaderInterface;
use App\Domain\Model\Interfaces\PartyInterface;
use App\Domain\Output\AnswerOutput;
use App\Domain\Output\AnswersOutput;
use App\Domain\Output\Interfaces\AnswerOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\AnswerRepository;
use App\Domain\Repository\PartyRepository;
use Doctrine\ORM\NonUniqueResultException;

class AnswerLoader implements AnswerLoaderInterface
{
    /**
     * @var AnswerRepository
     */
    private $answerRepository;
    /**
     * @var PartyRepository
     */
    private $partyRepository;

    /**
     * AnswerLoader constructor.
     *
     * @param AnswerRepository $answerRepository
     * @param PartyRepository  $partyRepository
     */
    public function __construct(
        AnswerRepository $answerRepository,
        PartyRepository $partyRepository
    ) {
        $this->answerRepository = $answerRepository;
        $this->partyRepository = $partyRepository;
    }

    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return AnswerOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        if ($options['list']) {
            $parties = $this->partyRepository->loadPartiesByInstanceId($id);
            $answers = [];

            /** @var PartyInterface $party */
            foreach ($parties as $party) {
                $answers = array_merge($answers, $this->answerRepository->loadAnswersByPartyId($party->getId()));
            }

            return new AnswersOutput($answers);
        }

        return new AnswerOutput(
            $this->answerRepository->loadAnswerByQuestionIdAndPartyIdAndTeamId(
                $options['partyId'],
                $options['questionId'],
                $options['teamId']
            )
        );
    }
}
