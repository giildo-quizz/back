<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Updater\Interfaces\GameUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublishAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var GameUpdaterInterface
     */
    private $gameUpdater;

    /**
     * PublishAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param GameUpdaterInterface  $gameUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        GameUpdaterInterface $gameUpdater
    ) {
        $this->apiResponder = $apiResponder;
        $this->gameUpdater = $gameUpdater;
    }

    /**
     * @Route(
     *     path="/api/admin/games/{id}/publish",
     *     requirements={"id"="\d+"},
     *     methods={"POST"},
     *     name="game_publish"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function publish(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->gameUpdater->update($request->getContent(), $id), $request);
    }
}
