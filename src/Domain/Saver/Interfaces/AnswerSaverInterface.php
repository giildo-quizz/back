<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\AnswerOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface AnswerSaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return AnswerOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface;
}
