<?php

namespace App\Domain\Deleter;

use App\Domain\Deleter\Interfaces\QuestionDeleterInterface;
use App\Domain\Repository\QuestionRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class QuestionDeleter implements QuestionDeleterInterface
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * QuestionDeleter constructor.
     *
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete($id): void
    {
        $this->questionRepository->deleteQuestion($id);
    }
}
