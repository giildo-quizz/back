<?php


namespace App\Domain\Output;


use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\UserInterface;
use App\Domain\Output\Interfaces\UserOutputInterface;

class UserOutput implements UserOutputInterface
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * UserOutput constructor.
     *
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInterface|null
     */
    public function getItem(): ?ModelInterface
    {
        return $this->user;
    }
}