<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\QuestionDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\QuestionInterface;

interface QuestionBuilderInterface extends BuilderInterface
{
    /**
     * @param QuestionDTO $dto
     * @param array|null   $params
     *
     * @return QuestionBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return QuestionInterface
     */
    public function getModel(): ModelInterface;
}
