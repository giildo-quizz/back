<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @ORM\Table(name="security_user")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $directoryId;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lastName;

    /**
     * User constructor.
     *
     * @param string      $directoryId
     * @param string      $username
     * @param string|null $mail
     * @param string|null $firstName
     * @param string|null $lastName
     * @param array|null  $roles
     */
    public function __construct(
        string $directoryId,
        string $username,
        ?string $mail = null,
        ?string $firstName = null,
        ?string $lastName = null,
        ?array $roles = []
    ) {
        $this->directoryId = $directoryId;
        $this->username = $username;
        $this->mail = $mail;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        empty($roles)
            ? $this->roles = ['ROLE_USER']
            : $this->roles = $roles;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDirectoryId(): string
    {
        return $this->directoryId;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getMail(): ?string
    {
        return $this->mail;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}
