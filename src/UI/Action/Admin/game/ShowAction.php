<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Loader\Interfaces\GameLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var GameLoaderInterface
     */
    private $gameLoader;

    /**
     * ShowAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param GameLoaderInterface   $gameLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        GameLoaderInterface $gameLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->gameLoader = $gameLoader;
    }

    /**
     * @Route(
     *     path="/api/admin/games/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"GET"},
     *     name="game_show"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->gameLoader->load($id), $request);
    }
}
