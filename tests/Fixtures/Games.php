<?php

namespace App\Tests\Fixtures;

use App\Domain\Model\Game;
use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Question;

trait Games
{
    /**
     * @var GameInterface
     */
    protected $games1;
    /**
     * @var GameInterface
     */
    protected $games2;
    /**
     * @var GameInterface
     */
    protected $games3;

    public function constructGames(): void
    {
        $this->games1 = new Game(
            'Quizz1',
            'Quizz sur les stars1',
            [
                new Question('mcq', 'Quels star ?', ['responses' => ['B. Pitt', 'T. Cruise']]),
                new Question('video', 'Quelle musique ?', ['url' => 'https://www.youtube.com/ssdfsqd']),
            ]
        );

        $this->games2 = new Game(
            'Quizz2',
            'Quizz sur les stars2',
            [
                new Question('mcq', 'Quels star ?', ['responses' => ['B. Pitt', 'T. Cruise']]),
                new Question('video', 'Quelle musique ?', ['url' => 'https://www.youtube.com/ssdfsqd']),
            ]
        );

        $this->games3 = new Game(
            'Quizz3',
            'Quizz sur les stars3',
            [
                new Question('mcq', 'Quels star ?', ['responses' => ['B. Pitt', 'T. Cruise']]),
                new Question('video', 'Quelle musique ?', ['url' => 'https://www.youtube.com/ssdfsqd']),
            ]
        );
    }
}
