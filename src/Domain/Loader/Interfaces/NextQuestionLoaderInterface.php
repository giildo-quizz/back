<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface NextQuestionLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null $id - Identifiant de la question
     * @param array|null $options - contient l'identifiant de l'instance
     *
     * @return QuestionOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
