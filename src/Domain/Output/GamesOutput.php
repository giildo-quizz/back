<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Output\Interfaces\GamesOutputInterface;

class GamesOutput implements GamesOutputInterface
{
    /**
     * @var GameInterface[]
     */
    private $games;

    /**
     * GameOutput constructor.
     *
     * @param GameInterface[] $games
     */
    public function __construct(array $games)
    {
        $this->games = $games;
    }

    /**
     * @return GameInterface[]
     */
    public function getItems(): array
    {
        return $this->games;
    }
}
