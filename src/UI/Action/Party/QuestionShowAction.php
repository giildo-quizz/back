<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\QuestionLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var QuestionLoaderInterface
     */
    private $questionLoader;

    /**
     * QuestionShowAction constructor.
     *
     * @param ApiResponderInterface   $apiResponder
     * @param QuestionLoaderInterface $questionLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        QuestionLoaderInterface $questionLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->questionLoader = $questionLoader;
    }

    /**
     * @Route(
     *     path="/api/questions/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"GET"},
     *     name="question_show"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function questionShow(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->questionLoader->load($id), $request);
    }
}
