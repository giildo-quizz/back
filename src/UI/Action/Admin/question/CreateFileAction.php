<?php

namespace App\UI\Action\Admin\question;

use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateFileAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     */
    public function __construct(ApiResponderInterface $apiResponder)
    {
        $this->apiResponder = $apiResponder;
    }

    /**
     * @Route(
     *     path="/api/admin/questions/files",
     *     name="question_create_file",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('files');

        $file->move(
            dirname(__FILE__) . '/../../../../../../front/src/assets/img/questions',
            $file->getClientOriginalName()
        );

        return $this->apiResponder->response(
            null,
            $request,
            Response::HTTP_CREATED
        );
    }
}
