<?php

namespace App\Domain\Saver;

use App\Domain\Builder\Interfaces\QuestionBuilderInterface;
use App\Domain\DTO\QuestionDTO;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use App\Domain\Output\QuestionOutput;
use App\Domain\Repository\GameRepository;
use App\Domain\Repository\QuestionRepository;
use App\Domain\Saver\Interfaces\QuestionSaverInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuestionSaver implements QuestionSaverInterface
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;
    /**
     * @var GameRepository
     */
    private $gameRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var QuestionBuilderInterface
     */
    private $questionBuilder;

    /**
     * QuestionSaver constructor.
     *
     * @param QuestionRepository       $questionRepository
     * @param GameRepository           $gameRepository
     * @param SerializerInterface      $serializer
     * @param ValidatorInterface       $validator
     * @param QuestionBuilderInterface $questionBuilder
     */
    public function __construct(
        QuestionRepository $questionRepository,
        GameRepository $gameRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        QuestionBuilderInterface $questionBuilder
    ) {
        $this->questionRepository = $questionRepository;
        $this->gameRepository = $gameRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->questionBuilder = $questionBuilder;
    }

    /**
     * @param string $credentials
     *
     * @return QuestionOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface
    {
        /** @var QuestionDTO $questionDTO */
        $questionDTO = $this->serializer->deserialize($credentials, QuestionDTO::class, 'json');
        $errors = $this->validator->validate($questionDTO);

        // TODO : gérer les erreurs

        $game = $this->gameRepository->loadGameById($questionDTO->game);
        $question = $this->questionBuilder->build($questionDTO, ['game' => $game,])
                                          ->getModel();
        $this->questionRepository->saveQuestion($question);

        return new QuestionOutput($question);
    }
}
