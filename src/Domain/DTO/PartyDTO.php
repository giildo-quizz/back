<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\PartyDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PartyDTO implements PartyDTOInterface
{
    /**
     * @var int|null
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    public $teamId;
    /**
     * @var int|null
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    public $gameId;

    /**
     * PartyDTO constructor.
     *
     * @param int|null $teamId
     * @param int|null $gameId
     */
    public function __construct(
        ?int $teamId = null,
        ?int $gameId = null
    ) {
        $this->teamId = $teamId;
        $this->gameId = $gameId;
    }
}
