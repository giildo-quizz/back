<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Loader\Interfaces\GameLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListAction
{
    /**
     * @var ApiResponderInterface
     */
    private $responder;
    /**
     * @var GameLoaderInterface
     */
    private $gameLoader;

    /**
     * ListAction constructor.
     *
     * @param ApiResponderInterface $responder
     * @param GameLoaderInterface   $gameLoader
     */
    public function __construct(
        ApiResponderInterface $responder,
        GameLoaderInterface $gameLoader
    ) {
        $this->responder = $responder;
        $this->gameLoader = $gameLoader;
    }

    /**
     * @Route(
     *     path="/api/admin/games",
     *     name="game_list",
     *     methods={"GET"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function list(Request $request): Response
    {
        return $this->responder->response($this->gameLoader->load(), $request);
    }
}
