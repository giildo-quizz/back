<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Saver\Interfaces\GameSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $responder;
    /**
     * @var GameSaverInterface
     */
    private $gameSaver;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface $responder
     * @param GameSaverInterface    $gameSaver
     */
    public function __construct(
        ApiResponderInterface $responder,
        GameSaverInterface $gameSaver
    ) {
        $this->responder = $responder;
        $this->gameSaver = $gameSaver;
    }

    /**
     * @Route(
     *     path="/api/admin/games",
     *     name="game_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function create(Request $request): Response
    {
        return $this->responder->response(
            $this->gameSaver->save($request->getContent()),
            $request,
            Response::HTTP_CREATED
        );
    }
}
