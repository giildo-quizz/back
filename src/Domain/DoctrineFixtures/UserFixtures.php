<?php


namespace App\Domain\DoctrineFixtures;


use App\Domain\Model\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $JaneDoe = new User(
            '886a3a5a-58d2-4ed4-916e-e47851c16c51',
            'JaneDoe',
            'jane@doe.fr',
            'Jane',
            'Doe',
            ['ROLE_ADMIN',]
        );
        $manager->persist($JaneDoe);
        $manager->flush();
    }
}