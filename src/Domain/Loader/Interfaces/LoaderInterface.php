<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;

interface LoaderInterface
{
    /**
     * @param int|null $id
     * @param array|null $options
     *
     * @return OutInterface|null
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
