<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\AnswerDTO;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Interfaces\AnswerInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface AnswerBuilderInterface extends BuilderInterface
{
    /**
     * @param AnswerDTO  $dto
     * @param array|null $params
     *
     * @return AnswerBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return AnswerInterface
     */
    public function getModel(): ModelInterface;
}
