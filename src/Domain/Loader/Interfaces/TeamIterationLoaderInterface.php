<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamsOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface TeamIterationLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null   $id - Identifiant de l'instance
     * @param array|null $options
     *
     * @return TeamsOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
