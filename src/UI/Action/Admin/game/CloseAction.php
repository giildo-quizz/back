<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Deleter\Interfaces\GameDeleterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CloseAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var GameDeleterInterface
     */
    private $gameDeleter;

    /**
     * CloseAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param GameDeleterInterface  $gameDeleter
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        GameDeleterInterface $gameDeleter
    ) {
        $this->apiResponder = $apiResponder;
        $this->gameDeleter = $gameDeleter;
    }

    /**
     * @Route(
     *     path="/api/admin/games/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"},
     *     name="game_close"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function close(
        Request $request,
        int $id
    ): Response {
        $this->gameDeleter->delete($id);

        return $this->apiResponder->response(null, $request);
    }
}
