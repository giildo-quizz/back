<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\QuestionBuilderInterface;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\QuestionDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\QuestionInterface;
use App\Domain\Model\Question;

class QuestionBuilder implements QuestionBuilderInterface
{
    /**
     * @var QuestionInterface
     */
    private $question;

    /**
     * @param QuestionDTO $dto
     * @param array|null           $params
     *
     * @return QuestionBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->question = new Question(
            $dto->type,
            $dto->text,
            $dto->definition,
            $params['game']
        );

        return $this;
    }

    /**
     * @return QuestionInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->question;
    }
}
