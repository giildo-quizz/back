<?php

namespace App\UI\Action\Admin\game;

use App\Domain\Loader\Interfaces\GameLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListPublishedAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var GameLoaderInterface
     */
    private $gameLoader;

    /**
     * ListPublishedAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param GameLoaderInterface   $gameLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        GameLoaderInterface $gameLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->gameLoader = $gameLoader;
    }

    /**
     * @Route(
     *     path="/api/admin/games/published",
     *     name="game_published_list",
     *     methods={"GET"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function list(Request $request): Response
    {
        return $this->apiResponder->response(
            $this->gameLoader->load(
                null,
                [
                    'published' => true,
                    'user'      => explode(' ', $request->headers->get('Authorization'))[1]
                ]
            ),
            $request
        );
    }
}
