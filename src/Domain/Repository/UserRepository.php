<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\UserInterface;
use App\Domain\Model\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $uid
     *
     * @return UserInterface|null
     * @throws NonUniqueResultException
     */
    public function loadUserByUid(string $uid): ?UserInterface
    {
        return $this->createQueryBuilder('u')
                    ->where('u.directoryId = :uid')
                    ->setParameter('uid', $uid)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param UserInterface $user
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveUser(UserInterface $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }
}
