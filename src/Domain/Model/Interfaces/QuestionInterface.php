<?php

namespace App\Domain\Model\Interfaces;

interface QuestionInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getText(): string;

    /**
     * @return string
     */
    public function getDefinition(): string;

    /**
     * @param string|null $text
     * @param string|null $definition
     */
    public function updateQuestion(
        ?string $text = '',
        ?string $definition = ''
    ): void;
}