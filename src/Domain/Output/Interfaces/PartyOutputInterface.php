<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\PartyInterface;

interface PartyOutputInterface extends OutputInterface
{
    /**
     * @return PartyInterface|null
     */
    public function getItem(): ?ModelInterface;
}
