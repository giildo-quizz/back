<?php

namespace App\Domain\Updater\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface QuestionUpdaterInterface extends UpdaterInterface
{
    /**
     * @param string $credentials
     * @param int $id
     *
     * @return QuestionOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface;
}
