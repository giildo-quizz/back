<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface QuestionLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return QuestionOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
