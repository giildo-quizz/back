<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\QuestionLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use App\Domain\Output\QuestionOutput;
use App\Domain\Repository\QuestionRepository;
use Doctrine\ORM\NonUniqueResultException;

class QuestionLoader implements QuestionLoaderInterface
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * QuestionLoader constructor.
     *
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return QuestionOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        return new QuestionOutput($this->questionRepository->loadQuestionById($id));
    }
}
