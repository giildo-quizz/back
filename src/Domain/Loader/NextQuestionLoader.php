<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\NextQuestionLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use App\Domain\Output\QuestionOutput;
use App\Domain\Repository\InstanceRepository;
use Doctrine\ORM\NonUniqueResultException;

class NextQuestionLoader implements NextQuestionLoaderInterface
{
    /**
     * @var InstanceRepository
     */
    private $instanceRepository;

    /**
     * NextQuestionLoader constructor.
     *
     * @param InstanceRepository $instanceRepository
     */
    public function __construct(InstanceRepository $instanceRepository)
    {
        $this->instanceRepository = $instanceRepository;
    }

    /**
     * @param int|null   $id - Identifiant de la question
     * @param array|null $options - contient l'identifiant de l'instance
     *
     * @return QuestionOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        $instance = $this->instanceRepository->loadInstanceById($options['instanceId']);

        /**
         * Si l'instance n'a pas de question associé l'admin n'a pas encore coché de question
         */
        if (is_null($instance->getQuestion())) {
            return null;
        }

        /**
         * Si l'utilisateur n'a pas d'ID de question il est sur la page Wait,
         * et si l'instance a un ID c'est que l'admin a coché une question
         */
        if ($id === 0 && !is_null($instance->getQuestion())) {
            return new QuestionOutput($instance->getQuestion());
        }

        /**
         * Sinon on retourne null si l'utilisateur a le même id que l'instance,
         * Si non on retourne la nouvelle question.
         */
        return $instance->getQuestion()->getId() !== $id ? new QuestionOutput($instance->getQuestion()) : null;
    }
}
