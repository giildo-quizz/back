<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\TeamInterface;

interface TeamOutputInterface extends OutputInterface
{
    /**
     * @return TeamInterface|null
     */
    public function getItem(): ?ModelInterface;
}
