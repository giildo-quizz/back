<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\QuestionInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Game
 * @ORM\Table(name="api_game")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\GameRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Game implements GameInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;
    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $createdAt;
    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $updatedAt;
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default"="false"})
     */
    private $closed;
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default"="false"})
     */
    private $published;
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default"="false"})
     */
    private $started;
    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="App\Domain\Model\Question", mappedBy="game", cascade={"persist", "remove"})
     */
    private $questions;

    /**
     * Game constructor.
     *
     * @param string              $title
     * @param string              $description
     * @param bool|null           $closed
     * @param bool|null           $published
     * @param bool|null           $started
     * @param QuestionInterface[] $questions
     */
    public function __construct(
        string $title,
        string $description,
        ?bool $closed = false,
        ?bool $published = false,
        ?bool $started = false,
        ?array $questions = []
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->closed = $closed;
        $this->published = $published;
        $this->started = $started;
        $this->questions = $questions;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    /**
     * @return bool
     */
    public function isStarted(): bool
    {
        return $this->started;
    }

    /**
     * @return QuestionInterface[]
     */
    public function getQuestions(): array
    {
        return $this->questions->getValues();
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param string|null $title
     * @param string|null $description
     */
    public function updateGame(
        ?string $title = '',
        ?string $description = ''
    ): void {
        $this->title = $title !== '' ? $title : $this->title;
        $this->description = $description !== '' ? $description : $this->description;
    }

    /**
     * @return void
     */
    public function close(): void
    {
        $this->closed = true;
    }

    /**
     * @param bool $isPublished
     *
     * @return void
     */
    public function changePublishStatus(bool $isPublished): void
    {
        $this->published = $isPublished;
    }

    /**
     * @ORM\PrePersist
     */
    public function changeDates(): void
    {
        if ($this->createdAt === null) {
            $this->createdAt = new DateTime('now');
        }

        $this->updatedAt = new DateTime('now');
    }

    /**
     * @return void
     */
    public function start(): void
    {
        $this->started = true;
    }

    /**
     * @return void
     */
    public function stop(): void
    {
        $this->started = false;
    }
}
