<?php

namespace App\Domain\Updater;

use App\Domain\DTO\TeamDTO;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamOutputInterface;
use App\Domain\Output\TeamOutput;
use App\Domain\Repository\TeamRepository;
use App\Domain\Updater\Interfaces\TeamUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TeamUpdater implements TeamUpdaterInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TeamUpdater constructor.
     *
     * @param SerializerInterface $serializer
     * @param ValidatorInterface  $validator
     * @param TeamRepository      $teamRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        TeamRepository $teamRepository
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->teamRepository = $teamRepository;
    }

    /**
     * @param string $credentials
     * @param int    $id
     *
     * @return TeamOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface {
        /** @var TeamDTO $teamDTO */
        $teamDTO = $this->serializer->deserialize($credentials, TeamDTO::class, 'json');
        $errors = $this->validator->validate($teamDTO);

        // TODO: Gérer les erreurs

        $team = $this->teamRepository->loadTeamById($id);
        $team->updateTeam(
            $teamDTO->name,
            $teamDTO->gamers
        );
        $this->teamRepository->save($team);
        return new TeamOutput($team);
    }
}
