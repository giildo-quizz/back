<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\TeamInterface;
use App\Domain\Model\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    /**
     * @return array
     */
    public function loadTeams(): array
    {
        return $this->createQueryBuilder('t')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * @param TeamInterface $team
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(TeamInterface $team): void
    {
        $this->_em->persist($team);
        $this->_em->flush();
    }

    /**
     * @param int $id
     *
     * @return TeamInterface|null
     * @throws NonUniqueResultException
     */
    public function loadTeamById(int $id): ?TeamInterface
    {
        return $this->createQueryBuilder('t')
                    ->where('t.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteTeam(int $id): void
    {
        $this->_em->remove($this->loadTeamById($id));
        $this->_em->flush();
    }
}
