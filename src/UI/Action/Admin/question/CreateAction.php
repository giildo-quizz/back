<?php

namespace App\UI\Action\Admin\question;

use App\Domain\Saver\Interfaces\QuestionSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var QuestionSaverInterface
     */
    private $questionSaver;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface  $apiResponder
     * @param QuestionSaverInterface $questionSaver
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        QuestionSaverInterface $questionSaver
    ) {
        $this->apiResponder = $apiResponder;
        $this->questionSaver = $questionSaver;
    }

    /**
     * @Route(
     *     path="/api/admin/questions",
     *     name="question_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Request $request): Response
    {
        return $this->apiResponder->response(
            $this->questionSaver->save($request->getContent()),
            $request,
            Response::HTTP_CREATED
        );
    }
}
