<?php

namespace App\Application\Script;

use App\Application\Server\Websocket;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WebsocketCommand extends Command
{
    protected function configure()
    {
        $this->setName('websocket:run')
             ->setDescription('Lance le serveur de Websocket');
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $server = IoServer::factory(
            new HttpServer(new WsServer(new Websocket())),
            8002,
            '127.0.0.1'
        );

        $server->run();
    }
}
