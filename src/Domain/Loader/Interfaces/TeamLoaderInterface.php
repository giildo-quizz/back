<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\NonUniqueResultException;

interface TeamLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null $id
     * @param array|null $options
     *
     * @return OutInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}
