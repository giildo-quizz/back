<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\GameInterface;

interface GamesOutputInterface extends OutputsInterface
{
    /**
     * @return GameInterface[]
     */
    public function getItems(): array;
}
