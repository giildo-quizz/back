<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\GameDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class GameDTO implements GameDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     min="5",
     *     max="255",
     *     minMessage="Vous devez mettre un titre d'au moins {{ limit }} caractères.",
     *     minMessage="Le titre doit faire moins que {{ limit }} caractères."
     * )
     */
    public $title;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     min="5",
     *     minMessage="La description doit faire au moins {{ limit }} caractères."
     * )
     */
    public $description;
    /**
     * @var bool|null
     * @Assert\NotBlank()
     * @Assert\Type(type="bool")
     */
    public $closed;
    /**
     * @var bool|null
     * @Assert\NotBlank()
     * @Assert\Type(type="bool")
     */
    public $published;

    /**
     * GameDTO constructor.
     *
     * @param string|null $title
     * @param string|null $description
     * @param bool|null   $closed
     * @param bool|null   $published
     */
    public function __construct(
        ?string $title = '',
        ?string $description = '',
        ?bool $closed = false,
        ?bool $published = false
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->closed = $closed;
        $this->published = $published;
    }
}
