<?php


namespace App\Domain\DTO;


use App\Domain\DTO\Interfaces\InstanceDTOInterface;
use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\QuestionInterface;

class InstanceDTO implements InstanceDTOInterface
{
    /**
     * @var GameInterface
     */
    public $game;
    /**
     * @var QuestionInterface
     */
    public $question;

    /**
     * InstanceDTO constructor.
     *
     * @param GameInterface|null     $game
     * @param QuestionInterface|null $question
     */
    public function __construct(
        ?GameInterface $game = null,
        ?QuestionInterface $question = null
    ) {
        $this->game = $game;
        $this->question = $question;
    }
}