<?php

namespace App\UI\Action\Team;

use App\Domain\Loader\Interfaces\TeamLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamLoaderInterface
     */
    private $teamLoader;

    /**
     * ShowAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param TeamLoaderInterface   $teamLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamLoaderInterface $teamLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamLoader = $teamLoader;
    }

    /**
     * @Route(
     *     path="/api/teams/{id}",
     *     requirements={"id"="\d+"},
     *     name="team_show",
     *     methods={"GET"}
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->teamLoader->load($id), $request);
    }
}
