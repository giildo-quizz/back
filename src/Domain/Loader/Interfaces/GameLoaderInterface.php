<?php

namespace App\Domain\Loader\Interfaces;

use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\GamesOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\NonUniqueResultException;

interface GameLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null $id
     * @param array|null $options
     *
     * @return GamesOutputInterface|GameOutputInterface
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): OutInterface;
}
