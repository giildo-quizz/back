<?php

namespace App\Domain\Updater;

use App\Domain\DTO\QuestionDTO;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;
use App\Domain\Output\QuestionOutput;
use App\Domain\Repository\QuestionRepository;
use App\Domain\Updater\Interfaces\QuestionUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

class QuestionUpdater implements QuestionUpdaterInterface
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * QuestionUpdater constructor.
     *
     * @param QuestionRepository  $questionRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(
        QuestionRepository $questionRepository,
        SerializerInterface $serializer
    ) {
        $this->questionRepository = $questionRepository;
        $this->serializer = $serializer;
    }

    /**
     * @param string $credentials
     * @param int    $id
     *
     * @return QuestionOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface {
        /** @var QuestionDTO $questionDTO */
        $questionDTO = $this->serializer->deserialize($credentials, QuestionDTO::class, 'json');
        $question = $this->questionRepository->loadQuestionById($id);
        $question->updateQuestion(
            $questionDTO->text,
            $questionDTO->definition
        );
        $this->questionRepository->saveQuestion($question);
        return new QuestionOutput($question);
    }
}
