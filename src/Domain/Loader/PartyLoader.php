<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\PartyLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\PartyOutputInterface;
use App\Domain\Output\PartyOutput;
use App\Domain\Repository\PartyRepository;
use Doctrine\ORM\NonUniqueResultException;

class PartyLoader implements PartyLoaderInterface
{
    /**
     * @var PartyRepository
     */
    private $partyRepository;

    /**
     * PartyLoader constructor.
     *
     * @param PartyRepository $partyRepository
     */
    public function __construct(PartyRepository $partyRepository)
    {
        $this->partyRepository = $partyRepository;
    }

    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return PartyOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        return new PartyOutput($this->partyRepository->loadPartyById($id));
    }
}
