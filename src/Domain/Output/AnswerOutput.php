<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\AnswerInterface;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Output\Interfaces\AnswerOutputInterface;

class AnswerOutput implements AnswerOutputInterface
{
    /**
     * @var AnswerInterface|null
     */
    private $answer;

    /**
     * AnswerOutput constructor.
     *
     * @param AnswerInterface|null $answer
     */
    public function __construct(?AnswerInterface $answer = null)
    {
        $this->answer = $answer;
    }

    /**
     * @return AnswerInterface|null
     */
    public function getItem(): ?ModelInterface
    {
        return $this->answer;
    }
}
