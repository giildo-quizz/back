<?php


namespace App\Domain\Output\Interfaces;


use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface InstanceOutputInterface extends OutputInterface
{
    /**
     * @return InstanceInterface|null
     */
    public function getItem(): ?ModelInterface;
}