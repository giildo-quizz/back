<?php

namespace App\Domain\Loader;

use App\Domain\Loader\Interfaces\TeamLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\TeamOutput;
use App\Domain\Output\TeamsOutput;
use App\Domain\Repository\TeamRepository;
use Doctrine\ORM\NonUniqueResultException;

class TeamLoader implements TeamLoaderInterface
{
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TeamLoader constructor.
     *
     * @param TeamRepository $teamRepository
     */
    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * @param int|null $id
     * @param array|null $options
     *
     * @return OutInterface
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        return is_null($id)
            ? new TeamsOutput($this->teamRepository->loadTeams())
            : new TeamOutput($this->teamRepository->loadTeamById($id));
    }
}
