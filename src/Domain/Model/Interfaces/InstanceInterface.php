<?php

namespace App\Domain\Model\Interfaces;

use DateTime;

interface InstanceInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return GameInterface
     */
    public function getGame(): GameInterface;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime;

    /**
     * @return QuestionInterface|null
     */
    public function getQuestion(): ?QuestionInterface;

    /**
     * @return bool
     */
    public function isClosed(): bool;

    /**
     * @param QuestionInterface $question
     */
    public function updateQuestion(QuestionInterface $question): void;

    public function changeDates(): void;
}