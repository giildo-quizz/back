<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Output\Interfaces\GameOutputInterface;

class GameOutput implements GameOutputInterface
{
    /**
     * @var GameInterface
     */
    private $game;

    /**
     * GameOutput constructor.
     *
     * @param GameInterface $game
     */
    public function __construct(GameInterface $game)
    {
        $this->game = $game;
    }

    /**
     * @return GameInterface
     */
    public function getItem(): ModelInterface
    {
        return $this->game;
    }
}
