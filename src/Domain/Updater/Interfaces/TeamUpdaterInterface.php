<?php

namespace App\Domain\Updater\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamOutputInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface TeamUpdaterInterface extends UpdaterInterface
{
    /**
     * @param string $credentials
     * @param int    $id
     *
     * @return TeamOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface;
}
