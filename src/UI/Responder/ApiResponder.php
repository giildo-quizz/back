<?php

namespace App\UI\Responder;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\OutputsInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiResponder implements ApiResponderInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ApiResponder constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritDoc}
     */
    public function response(
        ?OutInterface $content = null,
        ?Request $request = null,
        ?int $status = 200,
        ?array $headers = []
    ): Response {
        $headers['Content-Type'] = 'application/json';
        $response = new Response(
            $content ? $this->serializer->serialize($content, 'json') : null,
            $status,
            $headers
        );

        if (!is_null($request) && $request->isMethodCacheable()) {
            $response->setEtag(md5($response->getContent()));
            $response->setPublic();
            $response->isNotModified($request);
        }

        return $response;
    }
}
