<?php

namespace App\UI\Action\Admin\question;

use App\Domain\Updater\Interfaces\QuestionUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UpdateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var QuestionUpdaterInterface
     */
    private $questionUpdater;

    /**
     * UpdateQuestion constructor.
     *
     * @param ApiResponderInterface    $apiResponder
     * @param QuestionUpdaterInterface $questionUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        QuestionUpdaterInterface $questionUpdater
    ) {
        $this->apiResponder = $apiResponder;
        $this->questionUpdater = $questionUpdater;
    }

    /**
     * @Route(
     *     path="/api/admin/questions/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"PATCH"},
     *     name="question_update"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->questionUpdater->update($request->getContent(), $id), $request);
    }
}
