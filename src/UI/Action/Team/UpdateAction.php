<?php

namespace App\UI\Action\Team;

use App\Domain\Updater\Interfaces\TeamUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UpdateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamUpdaterInterface
     */
    private $teamUpdater;

    /**
     * UpdateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param TeamUpdaterInterface  $teamUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamUpdaterInterface $teamUpdater
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamUpdater = $teamUpdater;
    }

    /**
     * @Route(
     *     path="/api/teams/{id}",
     *     requirements={"id"="\d+"},
     *     name="team_update",
     *     methods={"PUT", "PATCH"}
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->teamUpdater->update($request->getContent(), $id), $request);
    }
}
