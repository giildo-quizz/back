<?php


namespace App\Domain\Saver;


use App\Domain\Builder\Interfaces\InstanceBuilderInterface;
use App\Domain\DTO\InstanceDTO;
use App\Domain\Model\Game;
use App\Domain\Output\InstanceOutput;
use App\Domain\Output\Interfaces\InstanceOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\GameRepository;
use App\Domain\Repository\InstanceRepository;
use App\Domain\Saver\Interfaces\InstanceSaverInterface;
use App\Domain\Updater\Interfaces\GameStartUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

class InstanceSaver implements InstanceSaverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var InstanceBuilderInterface
     */
    private $instanceBuilder;
    /**
     * @var InstanceRepository
     */
    private $instanceRepository;
    /**
     * @var GameStartUpdaterInterface
     */
    private $gameStartUpdater;

    /**
     * InstanceSaver constructor.
     *
     * @param SerializerInterface       $serializer
     * @param InstanceBuilderInterface  $instanceBuilder
     * @param InstanceRepository        $instanceRepository
     * @param GameStartUpdaterInterface $gameStartUpdater
     */
    public function __construct(
        SerializerInterface $serializer,
        InstanceBuilderInterface $instanceBuilder,
        InstanceRepository $instanceRepository,
        GameStartUpdaterInterface $gameStartUpdater
    ) {
        $this->serializer = $serializer;
        $this->instanceBuilder = $instanceBuilder;
        $this->instanceRepository = $instanceRepository;
        $this->gameStartUpdater = $gameStartUpdater;
    }

    /**
     * @param string $credentials
     *
     * @return InstanceOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NonUniqueResultException
     */
    public function save(string $credentials): OutInterface
    {
        $instance = $this->instanceBuilder->build(
            new InstanceDTO($this->gameStartUpdater->update('start', $credentials)->getItem())
        )->getModel();

        $this->instanceRepository->save($instance);

        return new InstanceOutput($instance);
    }
}