<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;

interface SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return OutInterface
     */
    public function save(string $credentials): OutInterface;
}
