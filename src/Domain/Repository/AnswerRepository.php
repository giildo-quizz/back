<?php

namespace App\Domain\Repository;

use App\Domain\Model\Answer;
use App\Domain\Model\Interfaces\AnswerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class AnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Answer::class);
    }

    /**
     * @param int $partyId
     * @param int $questionId
     * @param int $teamId
     *
     * @return AnswerInterface|null
     * @throws NonUniqueResultException
     */
    public function loadAnswerByQuestionIdAndPartyIdAndTeamId(
        int $partyId,
        int $questionId,
        int $teamId
    ): ?AnswerInterface {
        return $this->createQueryBuilder('a')
                    ->where('a.party = :partyId')
                    ->andWhere('a.question = :questionId')
                    ->andWhere('a.team = :teamId')
                    ->setParameters(['partyId' => $partyId, 'questionId' => $questionId, 'teamId' => $teamId])
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param AnswerInterface $answer
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(AnswerInterface $answer): void
    {
        $this->_em->persist($answer);
        $this->_em->flush();
    }

    /**
     * @param int $partyId
     *
     * @return array
     */
    public function loadAnswersByPartyId(int $partyId): array
    {
        return $this->createQueryBuilder('a')
                    ->where('a.party = :party')
                    ->setParameter('party', $partyId)
                    ->getQuery()
                    ->getResult();
    }
}
