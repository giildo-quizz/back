<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\QuestionDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionDTO implements QuestionDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $text;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    public $type;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $definition;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    public $game;

    /**
     * QuestionDTO constructor.
     *
     * @param string $text
     * @param int    $type
     * @param string $definition
     * @param int    $game
     */
    public function __construct(
        ?string $text = '',
        ?int $type = null,
        ?string $definition = '',
        ?int $game = null
    ) {
        $this->text = $text;
        $this->type = $type;
        $this->definition = $definition;
        $this->game = $game;
    }
}
