<?php

namespace App\Domain\Builder;

use App\Domain\Builder\Interfaces\BuilderInterface;
use App\Domain\Builder\Interfaces\PartyBuilderInterface;
use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\PartyDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\PartyInterface;
use App\Domain\Model\Party;
use App\Domain\Repository\GameRepository;
use App\Domain\Repository\TeamRepository;
use Doctrine\ORM\NonUniqueResultException;

class PartyBuilder implements PartyBuilderInterface
{
    /**
     * @var PartyInterface
     */
    private $party;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * PartyBuilder constructor.
     *
     * @param TeamRepository $teamRepository
     * @param GameRepository $gameRepository
     */
    public function __construct(
        TeamRepository $teamRepository,
        GameRepository $gameRepository
    ) {
        $this->teamRepository = $teamRepository;
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param PartyDTO   $dto
     * @param array|null $params
     *
     * @return PartyBuilderInterface
     * @throws NonUniqueResultException
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface {
        $this->party = new Party(
            $this->teamRepository->loadTeamById($dto->teamId),
            $this->gameRepository->loadGameById($dto->gameId),
            false
        );

        return $this;
    }

    /**
     * @return PartyInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->party;
    }
}
