<?php

namespace App\UI\Action\game;

use App\Domain\Loader\GameLoader;
use App\Domain\Repository\GameRepository;
use App\Tests\Fixtures\Games;
use App\UI\Responder\ApiResponder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ListActionTest extends TestCase
{
    /**
     * @var ListAction
     */
    private $action;

    protected function setUp()
    {
        $this->constructGames();

        /** @var SerializerInterface|MockObject $serializer */
        $serializer = $this->createMock(SerializerInterface::class);
        $serializer->method('serialize')->willReturn(
            json_encode(
                [
                    $this->games1,
                    $this->games2,
                    $this->games3,
                ]
            )
        );

        /** @var GameRepository|MockObject $repository */
        $repository = $this->createMock(GameRepository::class);
        $repository->method('loadGames')->willReturn(
            [
                $this->games1,
                $this->games2,
                $this->games3,
            ]
        );

        $this->action = new ListAction(
            new ApiResponder($serializer),
            new GameLoader($repository)
        );
    }

    use Games;

    public function testTheActionReturnsResponseWithGameInContent()
    {
        $response = $this->action->list();

        self::assertInstanceOf(Response::class, $response);
    }
}
