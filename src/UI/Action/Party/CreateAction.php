<?php

namespace App\UI\Action\Party;

use App\Domain\Saver\Interfaces\PartySaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PartySaverInterface
     */
    private $partySaver;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PartySaverInterface   $partySaver
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PartySaverInterface $partySaver
    ) {
        $this->apiResponder = $apiResponder;
        $this->partySaver = $partySaver;
    }

    /**
     * @Route(
     *     path="/api/parties",
     *     name="party_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Request $request): Response
    {
        return $this->apiResponder->response($this->partySaver->save($request->getContent()), $request);
    }
}
