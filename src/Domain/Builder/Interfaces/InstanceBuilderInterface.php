<?php


namespace App\Domain\Builder\Interfaces;


use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface InstanceBuilderInterface extends BuilderInterface
{
    /**
     * @param InstanceDTO $dto
     * @param array|null   $params
     *
     * @return InstanceBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return InstanceInterface
     */
    public function getModel(): ModelInterface;
}