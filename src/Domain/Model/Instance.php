<?php


namespace App\Domain\Model;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\QuestionInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Class Instance
 * @ORM\Table(name="api_instance")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\InstanceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Instance implements InstanceInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var GameInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Game", cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;
    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $createdAt;
    /**
     * @var QuestionInterface|null
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Question")
     */
    private $question;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $closed;

    /**
     * Instance constructor.
     *
     * @param GameInterface          $game
     * @param QuestionInterface|null $question
     * @param bool|null              $closed
     */
    public function __construct(
        GameInterface $game,
        ?QuestionInterface $question = null,
        ?bool $closed = false
    ) {
        $this->game = $game;
        $this->question = $question;
        $this->closed = $closed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return GameInterface
     */
    public function getGame(): GameInterface
    {
        return $this->game;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return QuestionInterface|null
     */
    public function getQuestion(): ?QuestionInterface
    {
        return $this->question;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @param QuestionInterface $question
     */
    public function updateQuestion(QuestionInterface $question): void
    {
        $this->question = $question;
    }

    /**
     * @ORM\PrePersist
     */
    public function changeDates(): void
    {
        if ($this->createdAt === null) {
            $this->createdAt = new DateTime('now');
        }
    }
}