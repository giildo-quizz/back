<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\PartyLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var PartyLoaderInterface
     */
    private $partyLoader;

    /**
     * ShowAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param PartyLoaderInterface  $partyLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        PartyLoaderInterface $partyLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->partyLoader = $partyLoader;
    }

    /**
     * @Route(
     *     path="/api/parties/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"GET"},
     *     name="party_show"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->partyLoader->load($id), $request);
    }
}
