<?php

namespace App\Application\Server;

use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;

class Websocket implements MessageComponentInterface
{
    /**
     * @var SplObjectStorage
     */
    private $clients;

    /**
     * Websocket constructor.
     */
    public function __construct()
    {
        $this->clients = new SplObjectStorage();
    }

    /**
     * When a new connection is opened it will be passed to this method
     *
     * @param ConnectionInterface $connection The socket/connection that just connected to your application
     *
     * @throws Exception
     */
    function onOpen(ConnectionInterface $connection)
    {
        $this->clients->attach($connection);
        $connection->send("Nouvelle connexion: Bonjour {$connection->resourceId}");
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $connection will
     * not result in an error if it has already been closed.
     *
     * @param ConnectionInterface $closedConnection
     */
    function onClose(ConnectionInterface $closedConnection)
    {
        $this->clients->detach($closedConnection);
        echo "La connexion {$closedConnection->resourceId} a été déconnectée.";
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through
     * this method
     *
     * @param ConnectionInterface $connection
     * @param Exception           $exception
     */
    function onError(
        ConnectionInterface $connection,
        Exception $exception
    ) {
        $connection->send('Une erreur est survenue : ' . $exception->getMessage());
        $connection->close();
    }

    /**
     * Triggered when a client sends data through the socket
     *
     * @param ConnectionInterface $connection
     * @param                     $message
     */
    function onMessage(
        ConnectionInterface $connection,
        $message
    ) {
        $totalClients = count($this->clients) - 1;
        echo vsprintf(
            'La connexion #%1$d envoie le message "%2$s" à %3$d autre%4$s connexion%5$s.' . "\n", [
            $connection->resourceId,
            $message,
            $totalClients,
            $totalClients === 1 ? '' : 's',
            $totalClients === 1 ? '' : 's',
        ]);

        foreach ($this->clients as $client) {
            if ($connection !== $client) {
                $client->send('Un autre joueur a répondu');
            }
        }
    }
}
