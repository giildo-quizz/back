<?php


namespace App\Domain\Loader;


use App\Application\Helper\JsonWebToken;
use App\Domain\Loader\Interfaces\UserLoaderInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\UserOutputInterface;
use App\Domain\Output\UserOutput;
use App\Domain\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;

class UserLoader implements UserLoaderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserLoader constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return UserOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface {
        return new UserOutput($this->userRepository->loadUserByUid((new JsonWebToken($options['user']))->getUid()));
    }
}