<?php

namespace App\Domain\Updater;

use App\Domain\DTO\GameDTO;
use App\Domain\Output\GameOutput;
use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\GameRepository;
use App\Domain\Updater\Interfaces\GameUpdaterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

class GameUpdater implements GameUpdaterInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameUpdater constructor.
     *
     * @param SerializerInterface $serializer
     * @param GameRepository      $gameRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        GameRepository $gameRepository
    ) {
        $this->serializer = $serializer;
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param string $credentials
     * @param int    $id
     *
     * @return GameOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface {
        /** @var GameDTO $gameDTO */
        $gameDTO = $this->serializer->deserialize($credentials, GameDTO::class, 'json');
        $game = $this->gameRepository->loadGameById($id);

        $gameDTO->title === '' && $gameDTO->description === ''
            ? $game->changePublishStatus($gameDTO->published)
            : $game->updateGame(
                $gameDTO->title,
                $gameDTO->description
            );

        $this->gameRepository->saveGame($game);
        return new GameOutput($game);
    }
}
