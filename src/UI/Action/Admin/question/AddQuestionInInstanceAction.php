<?php

namespace App\UI\Action\Admin\question;

use App\Domain\Updater\Interfaces\InstanceUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddQuestionInInstanceAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var InstanceUpdaterInterface
     */
    private $instanceUpdater;

    /**
     * AddQuestionInInstanceAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param InstanceUpdaterInterface $instanceUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        InstanceUpdaterInterface $instanceUpdater
    ) {
        $this->apiResponder = $apiResponder;
        $this->instanceUpdater = $instanceUpdater;
    }

    /**
     * @Route(
     *     path="/api/instance/{instanceId}/questions/{questionId}",
     *     requirements={"instanceId"="\d+", "questionId"="\d+"},
     *     methods={"PATCH"},
     *     name="instance_add_question"
     * )
     * @param Request $request
     * @param int     $instanceId
     * @param int     $questionId
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addQuestion(
        Request $request,
        int $instanceId,
        int $questionId
    ): Response {
        return $this->apiResponder->response($this->instanceUpdater->update($questionId, $instanceId), $request);
    }
}
