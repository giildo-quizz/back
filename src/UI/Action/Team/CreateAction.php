<?php

namespace App\UI\Action\Team;

use App\Domain\Saver\Interfaces\TeamSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamSaverInterface
     */
    private $teamSaver;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param TeamSaverInterface    $teamSaver
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamSaverInterface $teamSaver
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamSaver = $teamSaver;
    }

    /**
     * @Route(
     *     path="/api/teams",
     *     name="team_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        return $this->apiResponder->response(
            $this->teamSaver->save($request->getContent()),
            $request,
            Response::HTTP_CREATED
        );
    }
}
