<?php

declare(strict_types=1);

namespace App\Domain\DoctrineMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191112144441 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE api_game ADD COLUMN started BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('DROP INDEX IDX_AD001A30E48FD905');
        $this->addSql('DROP INDEX IDX_AD001A30296CD8AE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_party AS SELECT id, team_id, game_id, start_at, closed FROM api_party');
        $this->addSql('DROP TABLE api_party');
        $this->addSql('CREATE TABLE api_party (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, team_id INTEGER NOT NULL, game_id INTEGER NOT NULL, start_at DATE NOT NULL, closed BOOLEAN NOT NULL, CONSTRAINT FK_AD001A30296CD8AE FOREIGN KEY (team_id) REFERENCES api_team (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AD001A30E48FD905 FOREIGN KEY (game_id) REFERENCES api_game (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO api_party (id, team_id, game_id, start_at, closed) SELECT id, team_id, game_id, start_at, closed FROM __temp__api_party');
        $this->addSql('DROP TABLE __temp__api_party');
        $this->addSql('CREATE INDEX IDX_AD001A30E48FD905 ON api_party (game_id)');
        $this->addSql('CREATE INDEX IDX_AD001A30296CD8AE ON api_party (team_id)');
        $this->addSql('DROP INDEX IDX_65CC83C1E48FD905');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_question AS SELECT id, game_id, type, text, definition FROM api_question');
        $this->addSql('DROP TABLE api_question');
        $this->addSql('CREATE TABLE api_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, game_id INTEGER NOT NULL, type INTEGER NOT NULL, text CLOB NOT NULL COLLATE BINARY, definition CLOB NOT NULL COLLATE BINARY --(DC2Type:json_array)
        , CONSTRAINT FK_65CC83C1E48FD905 FOREIGN KEY (game_id) REFERENCES api_game (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO api_question (id, game_id, type, text, definition) SELECT id, game_id, type, text, definition FROM __temp__api_question');
        $this->addSql('DROP TABLE __temp__api_question');
        $this->addSql('CREATE INDEX IDX_65CC83C1E48FD905 ON api_question (game_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__api_game AS SELECT id, title, description, created_at, updated_at, closed, published FROM api_game');
        $this->addSql('DROP TABLE api_game');
        $this->addSql('CREATE TABLE api_game (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description CLOB NOT NULL, created_at DATE NOT NULL, updated_at DATE NOT NULL, closed BOOLEAN DEFAULT \'false\' NOT NULL, published BOOLEAN DEFAULT \'false\' NOT NULL)');
        $this->addSql('INSERT INTO api_game (id, title, description, created_at, updated_at, closed, published) SELECT id, title, description, created_at, updated_at, closed, published FROM __temp__api_game');
        $this->addSql('DROP TABLE __temp__api_game');
        $this->addSql('DROP INDEX IDX_AD001A30296CD8AE');
        $this->addSql('DROP INDEX IDX_AD001A30E48FD905');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_party AS SELECT id, team_id, game_id, start_at, closed FROM api_party');
        $this->addSql('DROP TABLE api_party');
        $this->addSql('CREATE TABLE api_party (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, team_id INTEGER NOT NULL, game_id INTEGER NOT NULL, start_at DATE NOT NULL, closed BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO api_party (id, team_id, game_id, start_at, closed) SELECT id, team_id, game_id, start_at, closed FROM __temp__api_party');
        $this->addSql('DROP TABLE __temp__api_party');
        $this->addSql('CREATE INDEX IDX_AD001A30296CD8AE ON api_party (team_id)');
        $this->addSql('CREATE INDEX IDX_AD001A30E48FD905 ON api_party (game_id)');
        $this->addSql('DROP INDEX IDX_65CC83C1E48FD905');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_question AS SELECT id, game_id, type, text, definition FROM api_question');
        $this->addSql('DROP TABLE api_question');
        $this->addSql('CREATE TABLE api_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, game_id INTEGER NOT NULL, type INTEGER NOT NULL, text CLOB NOT NULL, definition CLOB NOT NULL --(DC2Type:json_array)
        )');
        $this->addSql('INSERT INTO api_question (id, game_id, type, text, definition) SELECT id, game_id, type, text, definition FROM __temp__api_question');
        $this->addSql('DROP TABLE __temp__api_question');
        $this->addSql('CREATE INDEX IDX_65CC83C1E48FD905 ON api_question (game_id)');
    }
}
