<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\TeamInterface;
use App\Domain\Output\Interfaces\TeamsOutputInterface;

class TeamsOutput implements TeamsOutputInterface
{
    /**
     * @var TeamInterface[]
     */
    private $teams;

    /**
     * TeamsOutput constructor.
     *
     * @param TeamInterface[] $teams
     */
    public function __construct(array $teams)
    {
        $this->teams = $teams;
    }

    /**
     * @return TeamInterface[]
     */
    public function getItems(): array
    {
        return $this->teams;
    }
}
