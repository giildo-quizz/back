<?php

namespace App\UI\Action\Admin\question;

use App\UI\Responder\Interfaces\ApiResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteFileAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;

    /**
     * CreateAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     */
    public function __construct(ApiResponderInterface $apiResponder)
    {
        $this->apiResponder = $apiResponder;
    }

    /**
     * @Route(
     *     path="/api/admin/questions/files/delete",
     *     name="question_delete_file",
     *     methods={"POST"}
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function delete(Request $request): Response
    {
        $fileName = json_decode($request->getContent())->fileName;

        unlink(dirname(__FILE__) . '/../../../../../front/src/assets/img/questions/' . $fileName);

        return $this->apiResponder->response(
            null,
            $request,
            Response::HTTP_NO_CONTENT
        );
    }
}
