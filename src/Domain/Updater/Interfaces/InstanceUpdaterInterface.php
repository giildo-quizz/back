<?php

namespace App\Domain\Updater\Interfaces;

use App\Domain\Output\Interfaces\InstanceOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface InstanceUpdaterInterface extends UpdaterInterface
{
    /**
     * @param string     $credentials
     * @param int|string $id
     *
     * @return InstanceOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(
        string $credentials,
        $id
    ): OutInterface;
}
