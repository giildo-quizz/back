<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\AnswerInterface;
use App\Domain\Output\Interfaces\AnswersOutputInterface;

class AnswersOutput implements AnswersOutputInterface
{
    /**
     * @var AnswerInterface[]
     */
    private $answers;

    /**
     * AnswersOutput constructor.
     *
     * @param AnswerInterface[] $answers
     */
    public function __construct(array $answers)
    {
        $this->answers = $answers;
    }

    /**
     * @return AnswerInterface[]
     */
    public function getItems(): array
    {
        return $this->answers;
    }
}
