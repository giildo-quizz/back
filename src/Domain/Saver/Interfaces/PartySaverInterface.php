<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\PartyOutputInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface PartySaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return PartyOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface;
}
