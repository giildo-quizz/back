<?php

declare(strict_types=1);

namespace App\Domain\DoctrineMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110213438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE api_team (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(50) NOT NULL, gamers CLOB NOT NULL --(DC2Type:array)
        )');
        $this->addSql('DROP INDEX IDX_65CC83C1E48FD905');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_question AS SELECT id, game_id, type, text, definition FROM api_question');
        $this->addSql('DROP TABLE api_question');
        $this->addSql('CREATE TABLE api_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, game_id INTEGER NOT NULL, type INTEGER NOT NULL, text CLOB NOT NULL COLLATE BINARY, definition CLOB NOT NULL COLLATE BINARY --(DC2Type:json_array)
        , CONSTRAINT FK_65CC83C1E48FD905 FOREIGN KEY (game_id) REFERENCES api_game (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO api_question (id, game_id, type, text, definition) SELECT id, game_id, type, text, definition FROM __temp__api_question');
        $this->addSql('DROP TABLE __temp__api_question');
        $this->addSql('CREATE INDEX IDX_65CC83C1E48FD905 ON api_question (game_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE api_team');
        $this->addSql('DROP INDEX IDX_65CC83C1E48FD905');
        $this->addSql('CREATE TEMPORARY TABLE __temp__api_question AS SELECT id, game_id, type, text, definition FROM api_question');
        $this->addSql('DROP TABLE api_question');
        $this->addSql('CREATE TABLE api_question (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, game_id INTEGER NOT NULL, type INTEGER NOT NULL, text CLOB NOT NULL, definition CLOB NOT NULL --(DC2Type:json_array)
        )');
        $this->addSql('INSERT INTO api_question (id, game_id, type, text, definition) SELECT id, game_id, type, text, definition FROM __temp__api_question');
        $this->addSql('DROP TABLE __temp__api_question');
        $this->addSql('CREATE INDEX IDX_65CC83C1E48FD905 ON api_question (game_id)');
    }
}
