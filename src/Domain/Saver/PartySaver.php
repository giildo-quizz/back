<?php

namespace App\Domain\Saver;

use App\Domain\Builder\Interfaces\PartyBuilderInterface;
use App\Domain\DTO\PartyDTO;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\PartyOutputInterface;
use App\Domain\Output\PartyOutput;
use App\Domain\Repository\InstanceRepository;
use App\Domain\Repository\PartyRepository;
use App\Domain\Saver\Interfaces\PartySaverInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PartySaver implements PartySaverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var PartyBuilderInterface
     */
    private $partyBuilder;
    /**
     * @var PartyRepository
     */
    private $partyRepository;
    /**
     * @var InstanceRepository
     */
    private $instanceRepository;

    /**
     * PartySaver constructor.
     *
     * @param SerializerInterface   $serializer
     * @param ValidatorInterface    $validator
     * @param PartyBuilderInterface $partyBuilder
     * @param PartyRepository       $partyRepository
     * @param InstanceRepository    $instanceRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        PartyBuilderInterface $partyBuilder,
        PartyRepository $partyRepository,
        InstanceRepository $instanceRepository
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->partyBuilder = $partyBuilder;
        $this->partyRepository = $partyRepository;
        $this->instanceRepository = $instanceRepository;
    }

    /**
     * @param string $credentials
     *
     * @return PartyOutputInterface
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface
    {
        /** @var PartyDTO $partyDTO */
        $partyDTO = $this->serializer->deserialize($credentials, PartyDTO::class, 'json');
        $error = $this->validator->validate($partyDTO);

        // TODO: gestion des erreurs

        $party = $this->partyBuilder->build($partyDTO)->getModel();
        $party->addInstance($this->instanceRepository->loadInstanceByGameId($party->getGame()->getId()));
        $this->partyRepository->save($party);
        return new PartyOutput($party);
    }
}
