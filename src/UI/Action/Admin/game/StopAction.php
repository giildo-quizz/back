<?php


namespace App\UI\Action\Admin\game;


use App\Domain\Updater\Interfaces\GameStartUpdaterInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StopAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var GameStartUpdaterInterface
     */
    private $gameStartUpdater;

    /**
     * StartAction constructor.
     *
     * @param ApiResponderInterface     $apiResponder
     * @param GameStartUpdaterInterface $gameStartUpdater
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        GameStartUpdaterInterface $gameStartUpdater
    )
    {
        $this->apiResponder = $apiResponder;
        $this->gameStartUpdater = $gameStartUpdater;
    }

    /**
     * @Route(
     *     path="/api/admin/games/{id}/stop",
     *     requirements={"id"="\d+"},
     *     methods={"PATCH"},
     *     name="game_stop_update"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     */
    public function start(Request $request, int $id): Response
    {
        return $this->apiResponder->response($this->gameStartUpdater->update('stop', $id), $request);
    }
}