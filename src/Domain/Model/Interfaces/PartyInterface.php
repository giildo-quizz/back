<?php

namespace App\Domain\Model\Interfaces;

use DateTime;

interface PartyInterface extends ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return TeamInterface
     */
    public function getTeam(): TeamInterface;

    /**
     * @return GameInterface
     */
    public function getGame(): GameInterface;

    /**
     * @return InstanceInterface
     */
    public function getInstance(): InstanceInterface;

    /**
     * @return DateTime
     */
    public function getStartAt(): DateTime;

    /**
     * @return bool
     */
    public function isClosed(): bool;

    /**
     * @param InstanceInterface $instance
     */
    public function addInstance(InstanceInterface $instance): void;

    public function start(): void;

    /**
     * @return void
     */
    public function close(): void;
}