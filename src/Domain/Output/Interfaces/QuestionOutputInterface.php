<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\QuestionInterface;

interface QuestionOutputInterface extends OutputInterface
{
    /**
     * @return QuestionInterface
     */
    public function getItem(): ModelInterface;
}
