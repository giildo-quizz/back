<?php

namespace App\Application\Helper;

use App\Application\Helper\Interfaces\ExceptionOutputInterface;

class ExceptionOutput implements ExceptionOutputInterface
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var int|null
     */
    private $status;

    /**
     * ExceptionOutput constructor.
     *
     * @param string $message
     * @param int    $status
     */
    public function __construct(
        string $message,
        ?int $status = null
    ) {
        $this->message = $message;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }
}
