<?php

namespace App\Domain\Repository;

use App\Domain\Model\Interfaces\PartyInterface;
use App\Domain\Model\Interfaces\TeamInterface;
use App\Domain\Model\Party;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class PartyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Party::class);
    }

    /**
     * @param PartyInterface $party
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(PartyInterface $party): void
    {
        $this->_em->persist($party);
        $this->_em->flush();
    }

    /**
     * @param int $id
     *
     * @return TeamInterface[]|array|null
     */
    public function loadPartiesNotClosedByGameId(int $id): array
    {
        return $this->createQueryBuilder('p')
                    ->where('p.game = :id')
                    ->andWhere('p.closed = :closed')
                    ->setParameters(['id' => $id, 'closed' => false])
                    ->leftJoin('p.team', 'team')
                    ->addSelect('team')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * @param int $id
     *
     * @return PartyInterface|null
     * @throws NonUniqueResultException
     */
    public function loadPartyById(int $id): ?PartyInterface
    {
        return $this->createQueryBuilder('p')
                    ->where('p.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @return PartyInterface[]
     */
    public function loadPartiesByInstanceId(int $id): array
    {
        return $this->createQueryBuilder('p')
                    ->where('p.instance = :instance')
                    ->setParameter('instance', $id)
                    ->getQuery()
                    ->getResult();
    }
}
