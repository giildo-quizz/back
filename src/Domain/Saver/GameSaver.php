<?php

namespace App\Domain\Saver;

use App\Domain\Builder\Interfaces\GameBuilderInterface;
use App\Domain\DTO\GameDTO;
use App\Domain\DTO\Interfaces\GameDTOInterface;
use App\Domain\Output\GameOutput;
use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\GameRepository;
use App\Domain\Saver\Interfaces\GameSaverInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GameSaver implements GameSaverInterface
{
    /**
     * @var GameRepository
     */
    private $gameRepository;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var GameBuilderInterface
     */
    private $gameBuilder;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * GameSaver constructor.
     *
     * @param GameRepository       $gameRepository
     * @param ValidatorInterface   $validator
     * @param GameBuilderInterface $gameBuilder
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        GameRepository $gameRepository,
        ValidatorInterface $validator,
        GameBuilderInterface $gameBuilder,
        SerializerInterface $serializer
    ) {
        $this->gameRepository = $gameRepository;
        $this->validator = $validator;
        $this->gameBuilder = $gameBuilder;
        $this->serializer = $serializer;
    }

    /**
     * @param string $credentials
     *
     * @return GameOutputInterface
     * @throws ORMException
     */
    public function save(string $credentials): OutInterface
    {
        /** @var GameDTOInterface $gameDTO */
        $gameDTO = $this->serializer->deserialize($credentials, GameDTO::class,'json');
        $errors = $this->validator->validate($gameDTO);

        // TODO : gérer les erreurs

        $game = $this->gameBuilder->build($gameDTO)->getModel();
        $this->gameRepository->saveGame($game);

        return new GameOutput($game);
    }
}
