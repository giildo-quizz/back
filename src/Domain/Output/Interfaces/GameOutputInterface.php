<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\ModelInterface;

interface GameOutputInterface extends OutputInterface
{
    /**
     * @return GameInterface
     */
    public function getItem(): ModelInterface;
}
