<?php


namespace App\Domain\Loader\Interfaces;


use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\UserOutputInterface;
use Doctrine\ORM\NonUniqueResultException;

interface UserLoaderInterface extends LoaderInterface
{
    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return UserOutputInterface|null
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): ?OutInterface;
}