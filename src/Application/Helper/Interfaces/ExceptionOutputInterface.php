<?php

namespace App\Application\Helper\Interfaces;

use App\Domain\Output\Interfaces\OutInterface;

interface ExceptionOutputInterface extends OutInterface
{
    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return int|null
     */
    public function getStatus(): ?int;
}
