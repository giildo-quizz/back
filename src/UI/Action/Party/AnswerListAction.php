<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\AnswerLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnswerListAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var AnswerLoaderInterface
     */
    private $answerLoader;

    /**
     * AnswerListAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param AnswerLoaderInterface $answerLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        AnswerLoaderInterface $answerLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->answerLoader = $answerLoader;
    }

    /**
     * @Route(
     *     path="/api/answers/instance/{instanceId}",
     *     requirements={"instanceId"="\d+"},
     *     methods={"GET"},
     *     name="answer_list"
     * )
     * @param Request $request
     * @param int     $instanceId
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function answerList(
        Request $request,
        int $instanceId
    ): Response {
        return $this->apiResponder->response(
            $this->answerLoader->load($instanceId, ['list' => true,]),
            $request
        );
    }
}
