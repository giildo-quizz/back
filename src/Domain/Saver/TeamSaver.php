<?php

namespace App\Domain\Saver;

use App\Domain\Builder\Interfaces\TeamBuilderInterface;
use App\Domain\DTO\TeamDTO;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Output\Interfaces\TeamOutputInterface;
use App\Domain\Output\TeamOutput;
use App\Domain\Repository\TeamRepository;
use App\Domain\Saver\Interfaces\TeamSaverInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TeamSaver implements TeamSaverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var TeamBuilderInterface
     */
    private $teamBuilder;
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TeamSaver constructor.
     *
     * @param SerializerInterface  $serializer
     * @param ValidatorInterface   $validator
     * @param TeamBuilderInterface $teamBuilder
     * @param TeamRepository       $teamRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        TeamBuilderInterface $teamBuilder,
        TeamRepository $teamRepository
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->teamBuilder = $teamBuilder;
        $this->teamRepository = $teamRepository;
    }

    /**
     * @param string $credentials
     *
     * @return TeamOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface
    {
        /** @var TeamDTO $teamDTO */
        $teamDTO = $this->serializer->deserialize($credentials, TeamDTO::class, 'json');
        $errors = $this->validator->validate($teamDTO);

        // TODO : gérer les erreurs

        $team = $this->teamBuilder->build($teamDTO)->getModel();
        $this->teamRepository->save($team);
        return new TeamOutput($team);
    }
}
