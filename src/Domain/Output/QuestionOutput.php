<?php

namespace App\Domain\Output;

use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\QuestionInterface;
use App\Domain\Output\Interfaces\QuestionOutputInterface;

class QuestionOutput implements QuestionOutputInterface
{
    /**
     * @var QuestionInterface
     */
    private $question;

    /**
     * GameOutput constructor.
     *
     * @param QuestionInterface $question
     */
    public function __construct(QuestionInterface $question)
    {
        $this->question = $question;
    }

    /**
     * @return QuestionInterface
     */
    public function getItem(): ModelInterface
    {
        return $this->question;
    }
}
