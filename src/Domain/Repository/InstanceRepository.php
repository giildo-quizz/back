<?php


namespace App\Domain\Repository;

use App\Domain\Model\Instance;
use App\Domain\Model\Interfaces\InstanceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class InstanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Instance::class);
    }

    /**
     * @param InstanceInterface $instance
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(InstanceInterface $instance): void
    {
        $this->_em->persist($instance);
        $this->_em->flush();
    }

    /**
     * @param int $id
     *
     * @return InstanceInterface|null
     * @throws NonUniqueResultException
     */
    public function loadInstanceById(int $id): ?InstanceInterface
    {
        return $this->createQueryBuilder('i')
                    ->where('i.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @return InstanceInterface|null
     * @throws NonUniqueResultException
     */
    public function loadInstanceByGameId(int $id): ?InstanceInterface
    {
        return $this->createQueryBuilder('i')
                    ->leftJoin('i.game', 'game')
                    ->addSelect('game')
                    ->where('game.id = :id')
                    ->andWhere('i.closed = :closed')
                    ->setParameters(['id' => $id, 'closed' => false,])
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}