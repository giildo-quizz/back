<?php

namespace App\Domain\DTO;

use App\Domain\DTO\Interfaces\AnswerDTOInterface;

class AnswerDTO implements AnswerDTOInterface
{
    /**
     * @var int
     */
    public $partyId;
    /**
     * @var int
     */
    public $questionId;
    /**
     * @var int
     */
    public $teamId;
    /**
     * @var string
     */
    public $value;

    /**
     * AnswerDTO constructor.
     *
     * @param int|null    $partyId
     * @param int|null    $questionId
     * @param int|null    $teamId
     * @param string|null $value
     */
    public function __construct(
        ?int $partyId = null,
        ?int $questionId = null,
        ?int $teamId = null,
        ?string $value = ''
    ) {
        $this->partyId = $partyId;
        $this->questionId = $questionId;
        $this->teamId = $teamId;
        $this->value = $value;
    }
}
