<?php

namespace App\Domain\Saver\Interfaces;

use App\Domain\Output\Interfaces\QuestionOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

interface QuestionSaverInterface extends SaverInterface
{
    /**
     * @param string $credentials
     *
     * @return QuestionOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface;
}
