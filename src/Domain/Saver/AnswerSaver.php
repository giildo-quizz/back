<?php

namespace App\Domain\Saver;

use App\Domain\Builder\Interfaces\AnswerBuilderInterface;
use App\Domain\DTO\AnswerDTO;
use App\Domain\Loader\Interfaces\AnswerLoaderInterface;
use App\Domain\Output\AnswerOutput;
use App\Domain\Output\Interfaces\AnswerOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\AnswerRepository;
use App\Domain\Saver\Interfaces\AnswerSaverInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

class AnswerSaver implements AnswerSaverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var AnswerBuilderInterface
     */
    private $answerBuilder;
    /**
     * @var AnswerRepository
     */
    private $answerRepository;
    /**
     * @var AnswerLoaderInterface
     */
    private $answerLoader;

    /**
     * AnswerSaver constructor.
     *
     * @param SerializerInterface    $serializer
     * @param AnswerBuilderInterface $answerBuilder
     * @param AnswerRepository       $answerRepository
     * @param AnswerLoaderInterface  $answerLoader
     */
    public function __construct(
        SerializerInterface $serializer,
        AnswerBuilderInterface $answerBuilder,
        AnswerRepository $answerRepository,
        AnswerLoaderInterface $answerLoader
    ) {
        $this->serializer = $serializer;
        $this->answerBuilder = $answerBuilder;
        $this->answerRepository = $answerRepository;
        $this->answerLoader = $answerLoader;
    }

    /**
     * @param string $credentials
     *
     * @return AnswerOutputInterface
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(string $credentials): OutInterface
    {
        /** @var AnswerDTO $answerDTO */
        $answerDTO = $this->serializer->deserialize($credentials, AnswerDTO::class, 'json');
        // TODO: gestion des erreurs

        $answer = $this->answerLoader->load(
            null,
            [
                'partyId'    => $answerDTO->partyId,
                'questionId' => $answerDTO->questionId,
                'teamId'     => $answerDTO->teamId,
                'list'       => false,
            ]
        )->getItem();

        is_null($answer)
            ? $answer = $this->answerBuilder->build($answerDTO)->getModel()
            : $answer->setValue($answerDTO->value);

        $this->answerRepository->save($answer);
        return new AnswerOutput($answer);
    }
}
