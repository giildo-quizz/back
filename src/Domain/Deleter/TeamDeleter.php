<?php

namespace App\Domain\Deleter;

use App\Domain\Deleter\Interfaces\TeamDeleterInterface;
use App\Domain\Repository\TeamRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class TeamDeleter implements TeamDeleterInterface
{
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TeamDeleter constructor.
     *
     * @param TeamRepository $teamRepository
     */
    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete($id): void
    {
        $this->teamRepository->deleteTeam($id);
    }
}
