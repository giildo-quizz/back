<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\TeamIterationLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamListAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var TeamIterationLoaderInterface
     */
    private $teamIterationLoader;

    /**
     * TeamListAction constructor.
     *
     * @param ApiResponderInterface        $apiResponder
     * @param TeamIterationLoaderInterface $teamIterationLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        TeamIterationLoaderInterface $teamIterationLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->teamIterationLoader = $teamIterationLoader;
    }

    /**
     * @Route(
     *     path="/api/parties/{id}/teams",
     *     requirements={"id"="\d+"},
     *     name="party_team_list",
     *     methods={"GET"}
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function teamList(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->teamIterationLoader->load($id), $request);
    }
}
