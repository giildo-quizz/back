<?php

namespace App\Application\Helper;

use Symfony\Component\Config\Definition\Exception\Exception;

class TokenException extends Exception
{
    public const NO_TOKEN = 100;
    public const TOKEN_INVALID = 101;
    public const TOKEN_EXPIRED = 102;
}
