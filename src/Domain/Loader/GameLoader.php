<?php

namespace App\Domain\Loader;

use App\Application\Helper\JsonWebToken;
use App\Domain\Loader\Interfaces\GameLoaderInterface;
use App\Domain\Output\GameOutput;
use App\Domain\Output\GamesOutput;
use App\Domain\Output\Interfaces\GameOutputInterface;
use App\Domain\Output\Interfaces\GamesOutputInterface;
use App\Domain\Output\Interfaces\OutInterface;
use App\Domain\Repository\GameRepository;
use App\Domain\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;

class GameLoader implements GameLoaderInterface
{
    /**
     * @var GameRepository
     */
    private $gameRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * GameLoader constructor.
     *
     * @param GameRepository $gameRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        GameRepository $gameRepository,
        UserRepository $userRepository
    ) {
        $this->gameRepository = $gameRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int|null   $id
     * @param array|null $options
     *
     * @return GamesOutputInterface|GameOutputInterface
     * @throws NonUniqueResultException
     */
    public function load(
        ?int $id = null,
        ?array $options = []
    ): OutInterface {
        if (array_key_exists('published', $options)) {
            return new GamesOutput(
                $this->gameRepository->loadGamePublished(
                    $this->userRepository->loadUserByUid((new JsonWebToken($options['user']))->getUid())->getRoles()
                )
            );
        }

        return is_null($id)
            ? new GamesOutput($this->gameRepository->loadGames())
            : new GameOutput($this->gameRepository->loadGameById($id));
    }
}
