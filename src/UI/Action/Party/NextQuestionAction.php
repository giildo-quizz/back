<?php

namespace App\UI\Action\Party;

use App\Domain\Loader\Interfaces\NextQuestionLoaderInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NextQuestionAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var NextQuestionLoaderInterface
     */
    private $nextQuestionLoader;

    /**
     * NextQuestionAction constructor.
     *
     * @param ApiResponderInterface $apiResponder
     * @param NextQuestionLoaderInterface $nextQuestionLoader
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        NextQuestionLoaderInterface $nextQuestionLoader
    ) {
        $this->apiResponder = $apiResponder;
        $this->nextQuestionLoader = $nextQuestionLoader;
    }

    /**
     * @Route(
     *     path="/api/parties/{partyId}/instance/{instanceId}/question/{questionId}",
     *     requirements={"partyId"="\d+", "instanceId"="\d+", "questionId"="\d+"},
     *     methods={"GET"},
     *     name="next_question_show"
     * )
     * @param Request $request
     * @param int     $partyId
     * @param int     $questionId
     * @param int     $instanceId
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function nextQuestion(
        Request $request,
        int $partyId,
        int $questionId,
        int $instanceId
    ): Response {
        $response = $this->nextQuestionLoader->load($questionId, ['instanceId' => $instanceId, 'partyId' => $partyId]);
        return is_null($response)
            ? $this->apiResponder->response(null, $request, Response::HTTP_NO_CONTENT)
            : $this->apiResponder->response($response, $request);
    }
}
