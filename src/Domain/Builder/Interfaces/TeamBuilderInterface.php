<?php

namespace App\Domain\Builder\Interfaces;

use App\Domain\DTO\Interfaces\DTOInterface;
use App\Domain\DTO\TeamDTO;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Model\Interfaces\TeamInterface;

interface TeamBuilderInterface extends BuilderInterface
{
    /**
     * @param TeamDTO    $dto
     * @param array|null $params
     *
     * @return TeamBuilderInterface
     */
    public function build(
        DTOInterface $dto,
        ?array $params = []
    ): BuilderInterface;

    /**
     * @return TeamInterface
     */
    public function getModel(): ModelInterface;
}
