<?php


namespace App\UI\Action\Admin\game;


use App\Domain\Saver\Interfaces\InstanceSaverInterface;
use App\UI\Responder\Interfaces\ApiResponderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StartAction
{
    /**
     * @var ApiResponderInterface
     */
    private $apiResponder;
    /**
     * @var InstanceSaverInterface
     */
    private $instanceSaver;

    /**
     * StartAction constructor.
     *
     * @param ApiResponderInterface     $apiResponder
     * @param InstanceSaverInterface    $instanceSaver
     */
    public function __construct(
        ApiResponderInterface $apiResponder,
        InstanceSaverInterface $instanceSaver
    ) {
        $this->apiResponder = $apiResponder;
        $this->instanceSaver = $instanceSaver;
    }

    /**
     * @Route(
     *     path="/api/admin/games/{id}/start",
     *     requirements={"id"="\d+"},
     *     methods={"PATCH"},
     *     name="game_start_update"
     * )
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NonUniqueResultException
     */
    public function start(
        Request $request,
        int $id
    ): Response {
        return $this->apiResponder->response($this->instanceSaver->save($id), $request);
    }
}