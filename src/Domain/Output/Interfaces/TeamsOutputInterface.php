<?php

namespace App\Domain\Output\Interfaces;

use App\Domain\Model\Interfaces\TeamInterface;

interface TeamsOutputInterface extends OutputsInterface
{
    /**
     * @return TeamInterface[]
     */
    public function getItems(): array;
}
