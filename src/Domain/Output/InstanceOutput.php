<?php


namespace App\Domain\Output;


use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\ModelInterface;
use App\Domain\Output\Interfaces\InstanceOutputInterface;

class InstanceOutput implements InstanceOutputInterface
{
    /**
     * @var InstanceInterface
     */
    private $instance;

    /**
     * InstanceOutput constructor.
     *
     * @param InstanceInterface $instance
     */
    public function __construct(InstanceInterface $instance)
    {
        $this->instance = $instance;
    }

    /**
     * @return InstanceInterface|null
     */
    public function getItem(): ?ModelInterface
    {
        return $this->instance;
    }
}