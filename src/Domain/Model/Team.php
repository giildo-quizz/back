<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\TeamInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Team
 * @ORM\Table(name="api_team")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\TeamRepository")
 */
class Team implements TeamInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @var array|string[]
     * @ORM\Column(type="array")
     */
    private $gamers;

    /**
     * Team constructor.
     *
     * @param string $name
     * @param array  $gamers
     */
    public function __construct(
        string $name,
        array $gamers
    ) {
        $this->name = $name;
        $this->gamers = $gamers;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array|string[]
     */
    public function getGamers(): array
    {
        return $this->gamers;
    }

    /**
     * @param string $name
     * @param array  $gamers
     */
    public function updateTeam(
        string $name,
        array $gamers
    ): void {
        $this->name = $name;
        $this->gamers = $gamers;
    }

    /**
     * @param string $gamer
     */
    protected function addGamer(string $gamer): void
    {
        $this->gamers[] = $gamer;
    }

    /**
     * @param string $gamer
     */
    protected function removeGamer(string $gamer): void
    {
        if (array_search($this->gamers[], $this->gamers)) {
            $this->gamers[] = array_splice(
                $this->gamers[],
                array_search($this->gamers[], $this->gamers),
                1
            );
        }
    }
}
