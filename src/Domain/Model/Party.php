<?php

namespace App\Domain\Model;

use App\Domain\Model\Interfaces\GameInterface;
use App\Domain\Model\Interfaces\InstanceInterface;
use App\Domain\Model\Interfaces\PartyInterface;
use App\Domain\Model\Interfaces\TeamInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Party
 * @ORM\Table(name="api_party")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\PartyRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Party implements PartyInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var TeamInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Team", cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $team;
    /**
     * @var GameInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Game", cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;
    /**
     * @var InstanceInterface
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Instance")
     * @ORM\JoinColumn(nullable=false)
     */
    private $instance;
    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $startAt;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $closed;

    /**
     * Party constructor.
     *
     * @param TeamInterface $team
     * @param GameInterface $game
     * @param bool|null     $closed
     */
    public function __construct(
        TeamInterface $team,
        GameInterface $game,
        ?bool $closed = false
    ) {
        $this->team = $team;
        $this->game = $game;
        $this->closed = $closed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return TeamInterface
     */
    public function getTeam(): TeamInterface
    {
        return $this->team;
    }

    /**
     * @return GameInterface
     */
    public function getGame(): GameInterface
    {
        return $this->game;
    }

    /**
     * @return InstanceInterface
     */
    public function getInstance(): InstanceInterface
    {
        return $this->instance;
    }

    /**
     * @return DateTime
     */
    public function getStartAt(): DateTime
    {
        return $this->startAt;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    public function addInstance(InstanceInterface $instance): void
    {
        $this->instance = $instance;
    }

    /**
     * @ORM\PrePersist
     */
    public function start(): void
    {
        if ($this->startAt === null) {
            $this->startAt = new DateTime('now');
        }
    }

    /**
     * @return void
     */
    public function close(): void
    {
        $this->closed = true;
    }
}
